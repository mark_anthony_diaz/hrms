@extends('template.master')

@section('CSS')
<!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-3.0.2/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('content')

<div class="row">
  <div class="col-lg-7 col-md-12 col-sm-12">
    <!-- STACKED BAR CHART -->
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title"><b>DAILY TIME RECORD (DTR)</b></h3>
              
                  
                </h3>
                <div class="card-tools">
                
                  <form method="POST" id="frm_dtr" enctype="multipart/form-data" action="{{ url('dtr/pdf') }}" target="_blank">  
                  {{ csrf_field() }}
              <div class="float-right" style="margin-right: 1%">

                <select class="form-control-sm" name="dtr_year" id="dtr_year" onchange="showDTR()">
                  <?php
                    for ($i = date('Y'); $i >= (date('Y') - 5) ; $i--) { 
                        echo "<option value='$i'>".$i."</option>";
                    }
                  ?>
                </select>

              </div>
                <div class="float-right" style="margin-right: 1%">
                <select class="form-control-sm" name="dtr_mon" id="dtr_mon" onchange="showDTR()">
                  <option selected value='1'>January</option>
                  <option value='2'>February</option>
                  <option value='3'>March</option>
                  <option value='4'>April</option>
                  <option value='5'>May</option>
                  <option value='6'>June</option>
                  <option value='7'>July</option>
                  <option value='8'>August</option>
                  <option value='9'>September</option>
                  <option value='10'>October</option>
                  <option value='11'>November</option>
                  <option value='12'>December</option>
                </select>
              </div>
              @if(Auth::user()->usertype == 'Marshal' || Auth::user()->usertype == 'Director')
              <br>
              <br>
              <div class="float-center" style="margin-right: 1%">

                <select class="form-control-sm" name="userid" id="userid" onchange="showDTR()">
                  @foreach(getStaffDivision() AS $divs)
                    <option value='{{ $divs->id }}'>{{ $divs->lname.', '.$divs->fname.' '.$divs->mname }}</option>
                  @endforeach
                </select>

              </div>
              @else
                  <input type="hidden" id="userid" name="userid" value="{{ Auth::user()->id }}">
              @endif
                <h3 class="card-title float-right" style="padding-right: 10px">
                    
                  </form>
                </div>
              </div>
              <div class="card-body">
                <p><b>{{ mb_strtoupper(Auth::user()->lname.", ".Auth::user()->fname." ".Auth::user()->mname) }}</b></p>
                <h5><b><center>{{ date('F',mktime(0, 0, 0, $data['mon'], 10))." ".$data['yr'] }}</b></center></h5>
                <p><a href="{{ url('update-weekly-schedule/'.date('m').'/'.date('Y') ) }}" class="btn btn-primary btn-sm"><i class="fas fa-calendar-alt"></i> Update Weekly Schedule</a></p>
                <table class="table table-bordered" style="font-size: 12px">
                  <thead style="text-align: center">
                    <th style="text-align: left">Day</th><th>AM In</th><th>AM Out</th><th>PM In</th><th>PM Out</th><th style="width:30%">Remarks</th>
                  </thead>
                  <tbody>
                    <?php

                    $emp = App\User::where('id',Auth::user()->id)->first();

                    echo formatDTRrow($data['mon'],$data['yr'],$emp);
                      
                    
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
  </div>
</div>

@endsection

@section('JS')

<!-- Sparkline -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('AdminLTE-3.0.2/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('AdminLTE-3.0.2/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

<!-- ChartJS -->
<script src="{{ asset('AdminLTE-3.0.2/plugins/chart.js/Chart.min.js') }}"></script>


<script>
$("#dtr_mon").val({{ $data['mon'] }});
$("#userid").val({{ Auth::user()->id }});
$("#dtr_year").val({{ $data['yr'] }});


function showDTR()
{
  mon = $("#dtr_mon").val();
  yr = $("#dtr_year").val();
  userid = $("#userid").val();

  window.location.replace("{{ url('staff/attendance') }}/"+mon+"/"+yr+"/"+userid);
}
</script>
@endsection
@if(Auth::user()->usertype == 'Marshal' && Auth::user()->division == 'O')
     @include('dtr.director.request-for-approval-oed')
@else
     @include('dtr.director.request-for-approval-division')
@endif
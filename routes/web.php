<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use PhpParser\Parser\Multiple;

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
// Route::post('/', 'HomeController@index');
Route::get('home/{mon}/{yr}/{empid}/{weeknum}', 'HomeController@index2');
Route::get('home3/{mon}/{yr}/{empid}/{weeknum}', 'HomeController@index3');


/***********PERSONNEL INFORMATION***********/

Route::get('personal-information/{tab}', 'PersonnelInformation\StaffController@index');

//CALENDAR
Route::get('calendar', 'Calendar\CalendarController@index');


/*------ADMINISTRATOR PAGES------*/
Route::get('admin/dashboard/{division}', 'PersonnelInformation\AdminController@dashboard');
Route::get('list-of-employees', 'PersonnelInformation\AdminController@index');
Route::get('archived-employees', 'PersonnelInformation\AdminController@archived');
Route::get('list-of-applicants/{plantilla}', 'PersonnelInformation\AdminController@applicants');
Route::get('dashboard-employee/{div}', 'PersonnelInformation\EmployeeAdminController@dashboard');
Route::get('dashboard-staff/{div}', 'PersonnelInformation\EmployeeAdminController@dashboard2');
Route::get('add-new-employee', 'PersonnelInformation\EmployeeAdminController@index');
Route::get('retiree/{div}', 'PersonnelInformation\AdminController@retiree');
Route::get('update-employee/{id}', 'PersonnelInformation\EmployeeAdminController@updateview');
Route::get('service-record', 'PersonnelInformation\AdminController@servicerecord');
Route::get('pis-library/{tab}', 'PersonnelInformation\AdminController@library');
Route::get('contract-of-service', 'PersonnelInformation\AdminController@jos');
Route::get('retiree/terminal-leave/{userid}', 'PersonnelInformation\AdminController@terminalv');

Route::get('invites/pdf/{id}', 'PersonnelInformation\PDFController@invites');

Route::post('hire-new-employee', 'PersonnelInformation\EmployeeAdminController@hire');
Route::post('hire-transfer-employee', 'PersonnelInformation\EmployeeAdminController@transferhire');
Route::post('add-new-hire-employee', 'PersonnelInformation\EmployeeAdminController@newhire');

Route::post('reverse-dtr', 'Maintenance\Maintenance@reversedtr');

//REQUEST FOR HIRING
Route::get('letter-of-request-list', 'PersonnelInformation\AdminController@requestHiring');
Route::post('request-for-hiring/action', 'PersonnelInformation\AdminController@requestAction');
Route::post('request-for-hiring/send-to-psb', 'PersonnelInformation\AdminController@requestAction2');
Route::post('request-for-hiring/approve', 'PersonnelInformation\AdminController@requestApprove');
Route::post('request-for-hiring/upload-psb', 'PersonnelInformation\AdminController@uploadpsb');

//APPLICANT
Route::post('applicants/upload-psycho', 'PersonnelInformation\AdminController@uploadPsycho');


//INVITATION
Route::get('invitation/select/{id}', 'PersonnelInformation\InvitationController@select');
Route::post('invitation/create', 'PersonnelInformation\InvitationController@create');
Route::post('invitation/delete', 'PersonnelInformation\InvitationController@delete');
Route::post('invitation/update', 'PersonnelInformation\InvitationController@update');
Route::post('invitation/assign', 'PersonnelInformation\InvitationController@assign');
Route::get('invitation/json/{id}', 'PersonnelInformation\InvitationController@json');
Route::post('invitation/preview-list', 'PersonnelInformation\PDFController@previewInvitation');

//PLANTILLA
Route::get('vacant-position', 'PersonnelInformation\PlantillaController@index');
Route::post('plantilla/create', 'PersonnelInformation\PlantillaController@create');
Route::post('plantilla/delete', 'PersonnelInformation\PlantillaController@delete');
Route::post('plantilla/update', 'PersonnelInformation\PlantillaController@update');
Route::post('plantilla/assign', 'PersonnelInformation\PlantillaController@assign');
Route::post('plantilla/repost', 'PersonnelInformation\PlantillaController@repost');
Route::get('plantilla/json/{id}', 'PersonnelInformation\PlantillaController@json');


//POST EMPLOYEE
Route::post('employee/create', 'PersonnelInformation\EmployeeAdminController@create');
Route::post('employee/delete', 'PersonnelInformation\EmployeeAdminController@delete');
Route::post('employee/update', 'PersonnelInformation\EmployeeAdminController@update');

//POST DIVISION
Route::post('division/create', 'PersonnelInformation\PISLibraryDivisionController@create');
Route::post('division/delete', 'PersonnelInformation\PISLibraryDivisionController@delete');
Route::post('division/update', 'PersonnelInformation\PISLibraryDivisionController@update');

//POST POSITION
Route::post('position/create', 'PersonnelInformation\PISLibraryPositionController@create');
Route::post('position/delete', 'PersonnelInformation\PISLibraryPositionController@delete');
Route::post('position/update', 'PersonnelInformation\PISLibraryPositionController@update');

//POST DESIGNATION
Route::post('designation/create', 'PersonnelInformation\PISLibraryDesignationController@create');
Route::post('designation/delete', 'PersonnelInformation\PISLibraryDesignationController@delete');
Route::post('designation/update', 'PersonnelInformation\PISLibraryDesignationController@update');

//POST EMPLOYMENT
Route::post('employment/create', 'PersonnelInformation\PISLibraryEmploymentController@create');
Route::post('employment/delete', 'PersonnelInformation\PISLibraryEmploymentController@delete');
Route::post('employment/update', 'PersonnelInformation\PISLibraryEmploymentController@update');

//POST EMPLOYMENT
Route::post('salary/upload', 'PersonnelInformation\PISLibrarySalary@upload');

//RESET PASSWORD
Route::post('reset-password', 'PersonnelInformation\EmployeeAdminController@resetpassword');

//CHANGE STATUS
Route::post('change-status', 'PersonnelInformation\EmployeeAdminController@changestatus');

//TRANSFER
Route::post('transfer-employee', 'PersonnelInformation\EmployeeAdminController@transfer');


//LEAVE
Route::get('leave/json/{id}', 'JSON@leave');


//PDF REPORT
// Route::get('pdf-print', 'PersonnelInformation\PDFController@index');
Route::post('pdf/service-record', 'PersonnelInformation\PDFController@servicerecord');
Route::get('pdf/pds/', 'PersonnelInformation\PDFController@pds');
Route::get('position-classification/{division}/{class}', 'PersonnelInformation\PDFController@positionClass');
Route::get('position-description/{division}/{class}', 'PersonnelInformation\PDFController@positionDesc');
Route::get('trainings-list/{division}', 'PersonnelInformation\PDFController@trainingsList');
Route::get('employee-education/{division}/{class}', 'PersonnelInformation\PDFController@educationClass');

//POST TRAINING
Route::post('training/create', 'PersonnelInformation\TrainingController@create');
Route::post('training/delete', 'PersonnelInformation\TrainingController@delete');
Route::post('training/update', 'PersonnelInformation\TrainingController@update');
Route::get('training/json/{id}', 'PersonnelInformation\TrainingController@json');

Route::post('training/request/create', 'PersonnelInformation\TrainingController@createrequest');

//POST TRAINING TEMP
Route::post('request-for-training/create', 'PersonnelInformation\TrainingTempController@create');
Route::post('request-for-training/action', 'PersonnelInformation\TrainingTempController@action');
Route::post('request-for-training/delete', 'PersonnelInformation\TrainingTempController@delete');
Route::post('request-for-training/update', 'PersonnelInformation\TrainingTempController@update');
Route::post('request-for-training/complete', 'PersonnelInformation\TrainingTempController@complete');
Route::get('request-for-training/json/{id}', 'PersonnelInformation\TrainingTempController@json');

//EMPOYEE BASIC INFO
Route::post('basicinfo/check', 'PersonnelInformation\BasicinfoController@check');
Route::post('basicinfo/create', 'PersonnelInformation\BasicinfoController@create');
Route::post('basicinfo/update', 'PersonnelInformation\BasicinfoController@update');
Route::get('basicinfo/json/{id}', 'PersonnelInformation\BasicinfoController@json');
Route::post('basicinfo/update-photo', 'PersonnelInformation\BasicinfoController@updatePhoto');

//EMPOYEE FAMILY
Route::post('family/check', 'PersonnelInformation\FamilyController@check');
Route::post('family/create', 'PersonnelInformation\FamilyController@create');
Route::post('family/update', 'PersonnelInformation\FamilyController@update');
Route::get('family/json/{id}', 'PersonnelInformation\FamilyController@json');

//EMPOYEE WORK
Route::post('education/create', 'PersonnelInformation\EducationController@create');
Route::post('education/update', 'PersonnelInformation\EducationController@update');
Route::post('education/delete', 'PersonnelInformation\EducationController@delete');
Route::get('education/json/{id}', 'PersonnelInformation\EducationController@json');

//EMPOYEE ADD INFO
Route::post('addinfo/check', 'PersonnelInformation\AddinfoController@check');
Route::post('addinfo/create', 'PersonnelInformation\AddinfoController@create');
Route::post('addinfo/update', 'PersonnelInformation\AddinfoController@update');
Route::get('addinfo/json/{id}', 'PersonnelInformation\AddinfoController@json');

//EMPOYEE ORGANIZATION
Route::post('organization/create', 'PersonnelInformation\OrganizationController@create');
Route::post('organization/update', 'PersonnelInformation\OrganizationController@update');
Route::post('organization/delete', 'PersonnelInformation\OrganizationController@delete');
Route::get('organization/json/{id}', 'PersonnelInformation\OrganizationController@json');

//EMPOYEE WORK
Route::post('work/create', 'PersonnelInformation\WorkController@create');
Route::post('work/update', 'PersonnelInformation\WorkController@update');
Route::post('work/delete', 'PersonnelInformation\WorkController@delete');
Route::get('work/json/{id}', 'PersonnelInformation\WorkController@json');

//EMPOYEE ELIGIBILITY
Route::post('eligibility/create', 'PersonnelInformation\EligibilityController@create');
Route::post('eligibility/update', 'PersonnelInformation\EligibilityController@update');
Route::post('eligibility/delete', 'PersonnelInformation\EligibilityController@delete');
Route::get('eligibility/json/{id}', 'PersonnelInformation\EligibilityController@json');

//EMPOYEE SKILL
Route::post('skill/create', 'PersonnelInformation\SkillController@create');
Route::post('skill/update', 'PersonnelInformation\SkillController@update');
Route::post('skill/delete', 'PersonnelInformation\SkillController@delete');
Route::get('skill/json/{id}', 'PersonnelInformation\SkillController@json');

//COMPETENCY SKILL
Route::post('competency/create', 'PersonnelInformation\CompetencyController@create');
Route::post('competency/update', 'PersonnelInformation\CompetencyController@update');
Route::post('competency/delete', 'PersonnelInformation\CompetencyController@delete');
Route::get('competency/json/{id}', 'PersonnelInformation\CompetencyController@json');

Route::post('competency-duty/create', 'PersonnelInformation\CompetencyDutyController@create');
Route::post('competency-duty/update', 'PersonnelInformation\CompetencyDutyController@update');
Route::post('competency-duty/delete', 'PersonnelInformation\CompetencyDutyController@delete');
Route::get('competency-duty/json/{id}', 'PersonnelInformation\CompetencyDutyController@json');

Route::post('competency-training/create', 'PersonnelInformation\CompetencyTrainingController@create');
Route::post('competency-training/update', 'PersonnelInformation\CompetencyTrainingController@update');
Route::post('competency-training/delete', 'PersonnelInformation\CompetencyTrainingController@delete');
Route::get('competency-training/json/{id}', 'PersonnelInformation\CompetencyTrainingController@json');

Route::post('core-competency/create', 'PersonnelInformation\CompetencyController@create2');
Route::post('core-competency/update', 'PersonnelInformation\CompetencyController@update2');
Route::post('core-competency/delete', 'PersonnelInformation\CompetencyController@delete2');
Route::get('core-competency/json/{id}', 'PersonnelInformation\CompetencyController@json2');

//EMPOYEE RECOGNITION
Route::post('recognition/create', 'PersonnelInformation\RecognitionController@create');
Route::post('recognition/update', 'PersonnelInformation\RecognitionController@update');
Route::post('recognition/delete', 'PersonnelInformation\RecognitionController@delete');
Route::get('recognition/json/{id}', 'PersonnelInformation\RecognitionController@json');

//EMPOYEE ASSOCIATION
Route::post('association/create', 'PersonnelInformation\AssociationController@create');
Route::post('association/update', 'PersonnelInformation\AssociationController@update');
Route::post('association/delete', 'PersonnelInformation\AssociationController@delete');
Route::get('association/json/{id}', 'PersonnelInformation\AssociationController@json');

//EMPOYEE REFERENCES
Route::post('reference/create', 'PersonnelInformation\ReferenceController@create');
Route::post('reference/update', 'PersonnelInformation\ReferenceController@update');
Route::post('reference/delete', 'PersonnelInformation\ReferenceController@delete');
Route::get('reference/json/{id}', 'PersonnelInformation\ReferenceController@json');

//EMPOYEE FILE
Route::post('file/create', 'PersonnelInformation\FileController@create');
Route::post('file/update', 'PersonnelInformation\FileController@update');
Route::post('file/delete', 'PersonnelInformation\FileController@delete');
Route::get('file/json/{id}', 'PersonnelInformation\FileController@json');


//EMPOYEE ADDRESS/CONTACT
Route::post('address/check', 'PersonnelInformation\AddressController@check');
Route::get('location/municipal/{prov_id}', 'PersonnelInformation\AddressController@municipal');
Route::get('location/barangay/{mun_id}', 'PersonnelInformation\AddressController@barangay');

//MARSHALL
Route::get('letter-request', 'PersonnelInformation\MarshalController@requestHiring');
Route::get('recruitment/list-of-applicants/{id}/{letterid}', 'PersonnelInformation\SharedController@applicants');
Route::post('request-for-hiring/update-applicants', 'PersonnelInformation\SharedController@updateapplicants');
Route::post('request-for-hiring/create', 'PersonnelInformation\RequestForHiringController@create');
Route::post('request-for-hiring/update', 'PersonnelInformation\RequestForHiringController@update');
Route::post('request-for-hiring/delete', 'PersonnelInformation\RequestForHiringController@delete');
Route::post('request-for-hiring/repost', 'PersonnelInformation\RequestForHiringController@repost');
Route::get('request-for-hiring/json/{id}', 'PersonnelInformation\RequestForHiringController@json');
Route::get('request-for-hiring-alert/json', 'PersonnelInformation\RequestForHiringController@alert');
Route::get('request-for-hiring-alert/clear', 'PersonnelInformation\RequestForHiringController@clear');
Route::post('recruitment/upload/vacancy-advice', 'PersonnelInformation\RequestForHiringController@upload');
Route::get('recruitment/history/{id}', 'PersonnelInformation\PDFController@hiringHistory');


//RECRUITMENT
Route::get('recruitment/index', 'PersonnelInformation\RecruitmentController@index');
Route::get('recruitment/list-vacant-position', 'PersonnelInformation\RecruitmentController@vacant');

//LETTER CLEARANCE
Route::get('recruitment/letter-approval', 'PersonnelInformation\RequestForHiringController@approval');
Route::post('recruitment/clearance', 'PersonnelInformation\RequestForHiringController@clearance');


//LEARNING AND DEVELOPMENT




//STAFF
Route::get('invitation/list', 'PersonnelInformation\StaffController@invitation');
Route::get('invitation/alert', 'PersonnelInformation\StaffController@invitationalert');
Route::post('invitation/answer', 'PersonnelInformation\StaffController@invitationanswer');


/***********SHARED PAGES***********/
Route::get('trainings/update', 'PersonnelInformation\TrainingTempController@trainings');

/***********END PERSONNEL INFORMATION***********/




/***********PAYROLL***********/

//PDF REPORT
Route::post('pdf/my-payslip', 'Payroll\PDFController@myPayslip');

/***********END PAYROLL***********/



/***********APPLICANTS***********/

Route::get('job-vacancies', 'ApplicantController@vacancy');
Route::get('apply/{letter}/{item}', 'ApplicantController@index');
Route::get('thank-you', 'ApplicantController@thankyou');
Route::get('list-of-applicants-for-psb/{token}', 'ApplicantController@list');
Route::post('send-application', 'ApplicantController@create');

/***********END APPLICANTS***********/


/***********CALL FOR SUBMISSION***********/
Route::get('submission/list', 'Submission\PageController@index');
Route::get('submission-list/division', 'Submission\PageController@index2');
Route::post('submission-list/update', 'Submission\Submission@update2');

Route::post('submission/create', 'Submission\Submission@create');
Route::post('submission/update', 'Submission\Submission@update');
Route::post('submission/delete', 'Submission\Submission@delete');
Route::get('submission/json/{id}', 'Submission\Submission@json');

Route::get('submission/list/training-report', 'Submission\PageController@trainingreport');
Route::get('submission/list/training-certificate', 'Submission\PageController@trainingcertificate');

/***********CALL FOR SUBMISSION***********/


/***********NOTIFICATION***********/
Route::get('notifications', 'NotificationController@index');
/***********NOTIFICATION***********/


/***********LEARNING AND DEVELOPMENT***********/
Route::get('learning-development/index', 'PersonnelInformation\LearningDevController@index');
Route::post('learning-development/call-for-hrd-plan', 'PersonnelInformation\LearningDevController@hrdplandivision');
Route::get('learning-development/division-hrd-list/{id}', 'PersonnelInformation\LearningDevController@jsondivhrd');
Route::get('learning-development/hrdc-hrd-list/{id}', 'PersonnelInformation\LearningDevController@jsonhrdchrd');
Route::post('learning-development/division-upload-hrd-plan', 'PersonnelInformation\MarshalController@divuploadhrd');
Route::post('learning-development/send-to-hrdc', 'PersonnelInformation\LearningDevController@sendtohrdc');
Route::get('learning-development/list-hrd-approval', 'PersonnelInformation\SharedController@hrdapprovallist');
Route::post('learning-development/hrd-approval', 'PersonnelInformation\SharedController@hrdapproval');
Route::post('learning-development/send-to-oed', 'PersonnelInformation\LearningDevController@sendtooed');
Route::post('learning-development/oed-upload-final', 'PersonnelInformation\SharedController@oedupload');
Route::get('learning-development/hrd-plan/{hrd_degree_id}/{hrd_plan_id}', 'PersonnelInformation\SharedController@hrdplan');
Route::get('learning-development/json/hrd-plan-degree/{id}', 'PersonnelInformation\SharedController@jsonhrdplandegree');
Route::post('learning-development/save-hrd-plan-degree', 'PersonnelInformation\SharedController@savehrddegree');
Route::post('learning-development/save-hrd-plan-non-degree', 'PersonnelInformation\SharedController@savehrdnondegree');
Route::post('learning-development/update-hrd-plan-degree', 'PersonnelInformation\SharedController@updatehrddegree');
Route::post('learning-development/delete-hrd-plan-degree', 'PersonnelInformation\SharedController@deletehrddegree');
Route::post('learning-development/delete-hrd-plan-non-degree', 'PersonnelInformation\SharedController@deletehrdnondegree');
Route::get('learning-development/print/hrd-plan-degree/{degreeid}', 'PersonnelInformation\PDFController@hrddegree');
Route::get('learning-development/print/hrd-plan-non-degree/{degreeid}', 'PersonnelInformation\PDFController@hrdnondegree');
Route::post('learning-development/submit-hrd-plan', 'PersonnelInformation\SharedController@hrdsubmit');
Route::get('learning-development/print/hrd-plan-consolidated/{hrd_id}', 'PersonnelInformation\PDFController@hrdconsolidated');
Route::get('learning-development/print/hrd-plan-consolidated-degree/{hrd_id}', 'PersonnelInformation\PDFController@hrdconsolidated2');

Route::get('learning-development/print/hrd-plan-monitoring-non-degree/{hrd_id}', 'PersonnelInformation\PDFController@monitoringnondegree');
Route::get('learning-development/print/hrd-plan-monitoring-degree', 'PersonnelInformation\PDFController@monitoringdegree');

Route::get('learning-development/hrd-plan-review/{id}', 'ApplicantController@hrdcreview');
Route::post('learning-development/submit-hrd-plan-review', 'ApplicantController@hrdcreviewsubmit');
Route::post('learning-development/close-hrd', 'PersonnelInformation\LearningDevController@closehrd');

Route::get('learning-development/hrd-degree-json/{id}', 'PersonnelInformation\LearningDevController@degreejson');
Route::post('learning-development/hrd-degree-update/', 'PersonnelInformation\LearningDevController@degreeupdate');

/***********TRAINING AND DEVELOPMENT***********/



/***********PERFORMANCE MANAGEMENT***********/

Route::get('performance/index', 'PersonnelInformation\PerformanceController@index');
Route::get('performance/dpcr/json/{year}/{period}', 'PersonnelInformation\PerformanceController@jsondpcr');
Route::get('performance/division', 'PersonnelInformation\PerformanceController@division');
Route::post('performance/ipcr/create', 'PersonnelInformation\PerformanceController@ipcrcreate');
Route::post('performance/dpcr/create', 'PersonnelInformation\PerformanceController@dpcrcreate');
Route::post('performance/dpcr/submit', 'PersonnelInformation\PerformanceController@dpcrsubmit');
Route::post('performance/ipcr-staff/create', 'PersonnelInformation\PerformanceController@ipcruploadstaff');



/***********PERFORMANCE MANAGEMENT***********/


/***********AWARDS AND RECOGNITION***********/

Route::get('rewards/index', 'PersonnelInformation\RewardsRecognitionController@index');


/***********AWARDS AND RECOGNITION***********/


// Route::get('test', function () {
//     $dt = "01-15-2021";

//     return $dt->Carbon::isWeekend();
// });


// Route::get('update-emp', function () {
//     $emp = new App\Employee_division;
//     $emp = $emp->orderBy('id')->get();

//     // return $emp;
//     foreach($emp as $emps)
//     x`//     	$user = new App\Plantilla;
//     	$user = $user
//     			->where('user_id', $emps->user_id)
//           		->update(['employment_id' => $emps->employment_id]);
//     }
// });

// Route::get('plantillas', function () {
//     $emp = new App\Plantillas_history;
//     $emp = $emp->groupBy('username')->get();

//     // return $emp;
//     foreach($emp as $emps)
//     {
//     	$emp2 = new App\Plantillas_history;
//     	$emp2 = $emp2
//     			->where('username',$emps->username)
//     			->orderBy('plantilla_date_from','desc')
//     			->first();

//     	// echo $emp2->username."-".$emp2->plantilla_date_from."<br/>";

//     	$emp3 = new App\Plantilla;
//     	$emp3->username = $emp2->username;
//     	$emp3->plantilla_item_number = $emp2->plantilla_item_number;
//     	$emp3->plantilla_division = $emp2->plantilla_division;
//     	$emp3->position_id = $emp2->position_id;
//     	$emp3->plantilla_step = $emp2->plantilla_step;
//     	$emp3->employment_id = $emp2->employment_id;
//     	$emp3->plantilla_salary = $emp2->plantilla_salary;
//     	$emp3->plantilla_date_from = $emp2->plantilla_date_from;
//     	$emp3->plantilla_date_to = $emp2->plantilla_date_to;
//     	$emp3->plantilla_remarks = $emp2->plantilla_remarks;
//     	$emp3->save();
//     }
// });

// Route::get('divisions', function () {
//     $emp = new App\Employee_division;
//     $emp = $emp->groupBy('username')->get();

//     // return $emp;
//     foreach($emp as $emps)
//     {
//     	$emp2 = new App\Employee_division;
//     	$emp2 = $emp2
//     			->where('username',$emps->username)
//     			->orderBy('emp_desig_from','desc')
//     			->first();

//     	// echo $emp2->username."-".$emp2->plantilla_date_from."<br/>";

//     	$emp3 = new App\User;
//     	$emp3 = $emp3->where('username', $emp2->username)
//           		     ->update(['division' => $emp2->division_id]);
//     }
// });

// Route::get('test', function () {
// 	return App\HRD_plan_staff::where('user_id',Auth::user()->id)->whereNull('submitted_at')->count();
// });

// Route::get('update-leave', function () {
//     $emp = App\User::get();

//     // return $emp;
//     foreach($emp as $emps)
//     {
//     	App\Employee_leave::where('empcode',$emps->username)
//           		     ->update(['user_id' => $emps->id]);
//     }
// });



//DOWNLOAD
Route::get('download-zip', 'ZipController@downloadZip');


/***********ATTENDANCE MONITORING***********/

Route::get('dtr/terminal', 'AttendanceMonitoring\DTRController@terminal');
Route::get('dtr/icos/{mon}/{year}/{id}', 'AttendanceMonitoring\DTRController@icosindex');
// Route::get('dtr/icos', 'AttendanceMonitoring\DTRController@icosindex');
Route::post('dtr/icos/wfh', 'AttendanceMonitoring\DTRController@icoswfh');

Route::get('dtr/emp/{mon}/{year}', 'AttendanceMonitoring\DTRController@emp');
Route::post('dtr/process', 'AttendanceMonitoring\DTRController@processDTR');
Route::post('dtr/final-process', 'AttendanceMonitoring\FinalProcess@index');
Route::post('dtr/edit-view', 'AttendanceMonitoring\DTRController@edit2');
Route::post('dtr/update', 'AttendanceMonitoring\DTRController@update');
Route::post('dtr/weekly-schedule', 'AttendanceMonitoring\DTRController@weeksched');
Route::post('dtr/weekly-schedule-edit', 'AttendanceMonitoring\DTRController@weekschededit');
Route::post('dtr/weekly-schedule-add', 'AttendanceMonitoring\DTRController@weekschedadd');
Route::get('dtr/json/weekly-schedule/{id}', 'AttendanceMonitoring\DTRController@schedule');

Route::get('update-weekly-schedule/{mon}/{yr}', 'PersonnelInformation\StaffController@weeksched');
Route::post('update-weekly-schedule-send', 'PersonnelInformation\StaffController@weekschedsend');


// Route::get('test', 'AttendanceMonitoring\RequestController@dt');

//GET LEAVE PENDING
Route::get('dtr/get-pending-leave/{id}', 'AttendanceMonitoring\LeaveController@getPending');
Route::get('dtr/cancel-leave/{id}', 'AttendanceMonitoring\LeaveController@cancelLeave');
Route::post('dtr/add-leave', 'AttendanceMonitoring\LeaveController@updateLeave');

//PDF REPORT
// Route::get('pdf/my-dtr/{date}', 'AttendanceMonitoring\PDFController@myDTR');
Route::post('pdf/my-dtr', 'AttendanceMonitoring\PDFController@myDTR');
Route::get('pdf/leave-form', 'PDF\LeaveForm@index');

//REQUEST FOR LEAVE/OT/TO

//REQUEST APPROVED/DISAPPROVE
// Route::post('request/action', 'AttendanceMonitoring\DirectorController@actionRequest');

Route::post('request/{type}', 'AttendanceMonitoring\RequestController@index');
Route::get('dtr/request-leave', 'AttendanceMonitoring\LeaveController@request');
Route::post('dtr/send-leave-request', 'AttendanceMonitoring\LeaveController2@send');
Route::post('dtr/print-leave', 'AttendanceMonitoring\RequestController@pdf');
Route::post('dtr/print-wfh', 'AttendanceMonitoring\RequestController@wfh');

Route::post('dtr/request-action-leave', 'AttendanceMonitoring\DirectorController@actionRequest');
Route::post('dtr/request-action-to', 'AttendanceMonitoring\DirectorController@actionRequest');
Route::post('dtr/request-action-ot', 'AttendanceMonitoring\DirectorController@actionRequest');

//TO
Route::get('dtr/request-for-to', 'AttendanceMonitoring\TOController@index');
Route::post('dtr/send-to-request', 'AttendanceMonitoring\TOController@send');
Route::post('dtr/print-to', 'AttendanceMonitoring\TOController@pdf');

//OT
Route::get('dtr/request-for-ot', 'AttendanceMonitoring\OTController@index');
Route::post('dtr/send-ot-request', 'AttendanceMonitoring\OTController@send');
Route::post('dtr/print-ot', 'AttendanceMonitoring\OTController@pdf');
Route::post('dtr/print-batch-ot', 'AttendanceMonitoring\OTController@batchPDF');

//JSON FOR EDITING
Route::get('request/json/{type}/{id}', 'AttendanceMonitoring\RequestController@json');

//WFH
Route::post('dtr/send-wfh-request', 'AttendanceMonitoring\LeaveController@wfh');

//PRINT PDF
Route::get('request/print/{reqid}', 'AttendanceMonitoring\RequestController@pdf');

//DIRECTOR

//LEAVE/OT/TO FOR APPROVAL
Route::get('request-for-approval', 'AttendanceMonitoring\DirectorController@request');
Route::get('director-trainings-list', 'PersonnelInformation\TrainingController@list');
Route::post('request-for-approval-submit', 'AttendanceMonitoring\DirectorController@approvedLeaveRequest');



//DTR MONITORING
// Route::get('dtr/monitoring/{mon}/{yr}/{userid}', 'AttendanceMonitoring\DTRController@monitor');
Route::post('dtr/monitoring', 'AttendanceMonitoring\DTRController@monitor');
Route::post('dtr/pdf', 'AttendanceMonitoring\DTRController@pdf');
Route::post('dtr-icos/pdf', 'AttendanceMonitoring\DTRController@pdficos');
Route::post('dtr/edit', 'AttendanceMonitoring\DTRController@edit');
// Route::get('dtr/edit/{id}/{mon}/{yr}', 'AttendanceMonitoring\DTRController@edit2');
Route::get('change-password', 'AttendanceMonitoring\DTRController@password');
Route::post('change-password-send', 'HomeController@changepassword');
Route::post('dtr/icos/update', 'AttendanceMonitoring\DTRController@icosupdate');
Route::post('dtr/icos/add', 'AttendanceMonitoring\DTRController@icoswfhto');

Route::post('dtr/edit/submit', 'AttendanceMonitoring\DTRController@editdtr');

Route::get('dtr/employee', 'AttendanceMonitoring\DTRController@empdtrmonth');

Route::get('dtr/process/{mon}/{yr}', 'AttendanceMonitoring\DTRController@proccessdtr');

Route::get('dtr/report', 'AttendanceMonitoring\AdminController@report');

Route::get('json/dtr/{userid}/{type}/{col}/{t}', 'JSON@dtr');

Route::get('dtr/process-json/{user}', 'AttendanceMonitoring\AdminController@dtrprocess');




//LEAVE
// Route::post('cams/apply-leave', 'AttendanceMonitoring\LeaveController@apply');

// Route::get('test', function () {
//    $division = App\Division::get();
//    foreach ($division as $divisions) {
   		
//    		for ($i=1; $i <= 12 ; $i++) { 
//    			$dtr = new App\DTRProcessed;
//    			$dtr->dtr_mon = $i;
//    			$dtr->dtr_year = 2021;
//    			$dtr->dtr_division = $divisions->division_id;
//    			$dtr->save();
//    		}
//    }
// });

// Route::get('test', function () {
//    return checkDTRStaff(2,2021,'K');
// });

Route::get('monitor-dtr/EzUt1cg19i/{datedtr}', function ($datedtr) {

   //TOTAL ICOS + REGULAR
    $total_emp = App\User::whereIn('employment_id',[1,5,8])->count();

    //GET ICOS ATTENDANCE
    $total_icos = App\Employee_icos_dtr::where('fldEmpDTRdate',$datedtr)->whereNull('dtr_remarks')->groupBy('fldEmpCode')->get();
    $total_icos = count($total_icos);

    //GET REGULAR ATTENDANCE
    $total_reg = App\Camsdtr::where('fldEmpDTRdate',$datedtr)->count();

    //PERCENTAGE
    $total = $total_icos + $total_reg;
    $percent = ($total / $total_emp) * 100;
    $percenttotal = number_format($percent, 0);

    $data = [
    			'datedtr' => $datedtr,
    			'total_reg' => $total_reg,
    			'total_icos' => $total_icos,
    			'percent' => $percenttotal
    		];
   return view('monitor-dtr')->with('data',$data);
});


// Route::get('test', function () {

//    return getLeaveInfo(1);
// });


/***********REPORT***********/

Route::post('dtr/report/dtr-summary', 'PDF\DTRSummary@index');
Route::post('dtr/report/daily-monitoring', 'PDF\DailyMonitoring@index');
Route::post('dtr/report/final-processed-dtr', 'PDF\FinalProcessedDTR@index');
Route::post('dtr/report/excessive-tardiness', 'PDF\ExcessiveTardiness@index');
Route::post('dtr/report/sala-attendance', 'PDF\SALAAttendance@index');
Route::post('dtr/report/hp-attendance', 'PDF\HPAttendance@index');
Route::post('dtr/report/travel-order', 'PDF\TravelOrder@index');
Route::post('dtr/report/leave-record', 'PDF\LeaveRecord@index');
Route::post('dtr/report/leave-without-pay', 'PDF\LeaveWoutPay@index');
Route::post('dtr/report/rendering-overtime', 'PDF\RenderingOvertime@index');
Route::post('dtr/print', 'PDF\DTR@index');


/***********CORE COMPETENCY***********/

Route::get('core-competency', 'PersonnelInformation\CompetencyController@core');


/***********MAINTENANCE***********/

Route::get('maintenance', 'Maintenance\Maintenance@index');
Route::post('maintenance/work-schedule/update', 'Maintenance\Maintenance@workschedule');
Route::post('maintenance/library/{action}', 'Maintenance\Maintenance@library');



/***********DASHBOARD***********/

Route::get('dashboard', 'Dashboard@index');





/***********PAYROLL***********/

Route::get('payroll/emp', 'Payroll\EmployeeInfo@index');
Route::get('payroll/mc/{mon}/{yr}', 'Payroll\Report@mc');
Route::get('payroll/mc-pending/json/{mon}/{yr}', 'Payroll\Report@mcpending');
Route::get('payroll/mc-deduc/json/{id}/{col}', 'Payroll\Report@mcdeduc');
Route::post('payroll/mc-deduc-edit', 'Payroll\Report@mcdeducedit');
Route::post('payroll/mc-process', 'Payroll\Process@mcprocess');

Route::get('payroll/emp/{empcode}', 'Payroll\EmployeeInfo@index2');

Route::get('payroll/library', 'Payroll\Library@index');
Route::get('payroll/report', 'Payroll\Report@index');

Route::get('payroll/salary-deduc-manda/json/{empcode}/{id}', 'Payroll\Report@deducjson');
Route::get('payroll/salary-deduc-loan/json/{empcode}/{id}', 'Payroll\Report@loanjson');
Route::post('payroll/salary-deduc-manda-loan-edit', 'Payroll\Report@deducmandaloan');


Route::get('payroll/remittance', 'Payroll\Report@remittance');
Route::post('payroll/remittance', 'Payroll\PDFController@printRemittance');


//PROCESS PAYROLL
Route::get('payroll/process', 'Payroll\Process@index');
Route::post('payroll/create', 'Payroll\Process@create');

Route::get('test-code', 'Payroll\Process@test');

//MC
Route::post('payroll/mc-report', 'Payroll\PDFController@MCreport');
Route::post('payroll/mc-print', 'Payroll\PDFController@printMC');

Route::get('payroll/json/{tbl}', 'Payroll\Library@json');
Route::post('payroll/preview', 'Payroll\PDFController@previewpayroll');


//ADD EMPTY ROWS FOR EMPLOYEE INFO
Route::get('add-info', function () {
   $user = App\User::get();


   foreach ($user as $users) {
   		
   		$tbl = App\Employee_addinfo::where('user_id',$users->id)->count();
   		if($tbl > 0)
   		{
   			$tbl = new App\Employee_addinfo;
   			$tbl->user_id = $users->id;
   			$tbl->save();
   		}

   		$tbl = App\Employee_address_permanent::where('user_id',$users->id)->count();
   		if($tbl > 0)
   		{
   			$tbl = new App\Employee_address_permanent;
   			$tbl->user_id = $users->id;
   			$tbl->save();
   		}

   		$tbl = App\Employee_family::where('user_id',$users->id)->count();
   		if($tbl > 0)
   		{
   			$tbl = new App\Employee_family;
   			$tbl->user_id = $users->id;
   			$tbl->save();
   		}

   		$tbl = App\Employee_contact::where('user_id',$users->id)->count();
   		if($tbl > 0)
   		{
   			$tbl = new App\Employee_contact;
   			$tbl->user_id = $users->id;
   			$tbl->save();
   		}

   }
});

Route::get('test-insert', function () {
	$data = collect(['division_acro' => 'HELLO']);
	$dt = App\Division::insertGetId($data->all());
	return $dt;
});

//NEW PAGES
Route::get('staff/attendance/{mon}/{y}/{userid}', 'PersonnelInformation\StaffController@attendance');
Route::get('staff/leave', 'PersonnelInformation\StaffController@leave');
Route::get('staff/leave/{id}', 'PersonnelInformation\StaffController@leave2');
Route::get('staff/to', 'PersonnelInformation\StaffController@to');
Route::get('staff/cto', 'PersonnelInformation\StaffController@cto');
Route::get('staff/json/cto/{id}', 'PersonnelInformation\StaffController@ctostaff');
Route::get('staff/payroll', 'PersonnelInformation\StaffController@payroll');
Route::get('staff/loan', 'PersonnelInformation\StaffController@loan');

//FOR TESTING
Route::get('staff/leave/test', 'PersonnelInformation\StaffController@leavetest');
Route::post('dtr/send-leave-request/test', 'AttendanceMonitoring\LeaveController2@send2');
Route::get('staff/leave-test/{id}', 'PersonnelInformation\StaffController@leave22');

Route::get('test', function () {
	
});

// use Illuminate\Support\Facades\Hash;
Route::get('update-password', function () {
	// $user = App\CamsUser::get();
	// foreach ($user as $key => $value) {
	// 	$emp = App\User::where('username',$value->fldUsername)
	// 			->update([
	// 						'password' => Hash::make($value->fldPassword)
	// 					]);
	// }

	return bcrypt('aleja123');
});

//Workforce Monitoring *maine
	Route::get('workforce-monitoring/{date}', function ($date) {
		$data = [
					"date" => $date,
				];
		return view('workforce-monitoring')->with("data",$data);
	});

	Route::get('workforce-monitoring', function () {
		$date = date('Y-m-d');

		return redirect('workforce-monitoring/'.$date);
	});


//gss2 *maine
	Route::get('gss2/{date}', function ($date) {
		$data = [	
					"date" => $date,
				];
		return view('gss2')->with("data",$data);
	});
	
	Route::get('gss2', function () {
		$date = date('Y-m-d');	
	
		return redirect('gss2/'.$date);
	});

//GSS
Route::get('gss', 'GSSController@index');
Route::get('gss/{mon}/{year}', 'GSSController@index2');

Route::post('update-weekly-schedule-details', 'PersonnelInformation\StaffController@updadetails');


Route::get('json/ctobal/{userid}', 'JSONController@ctobal');


Route::get('schedule-monitor/{mon}/{yr}/{week}/{div}', function ($mon,$yr,$wk,$div) {
	$data = [
				"division" => $div,
				"mon" => $mon,
				"yr" => $yr,
				"weeknum" => $wk
			];
	return view('schedule-monitor')->with("data",$data);
});

Route::get('schedule-monitor', function () {
	$mon = date('m');
	$yr = date('Y');
	$week = date(1);

	return redirect('schedule-monitor/'.$mon."/".$yr."/".$week."/K");
});


Route::get('revert-dtr', function () {
	return view('revert');
});

Route::get('add-leave', function () {
	return view('addleave');
});

Route::post('add-leave-post', function () {
	
	// $user = App\User::where('id',request()->userid)->first();

	// if(request()->lv_pl != "" || request()->lv_pl != null)
	// {
	// 	$lv = new App\Employee_leave;
	// 	$lv->leave_id = 3;
	// 	$lv->user_id = request()->userid;
	// 	$lv->empcode = $user['username'];
	// 	$lv->leave_bal = request()->lv_pl;
	// 	$lv->save();
	// }

	// if(request()->lv_fl != "" || request()->lv_fl != null)
	// {
	// 	$lv2 = new App\Employee_leave;
	// 	$lv2->leave_id = 6;
	// 	$lv2->user_id = request()->userid;
	// 	$lv2->empcode = $user['username'];
	// 	$lv2->leave_bal = request()->lv_fl;
	// 	$lv2->save();
	// }

	// return redirect('add-leave');
});

Route::get('delete/dtr-process/{id}', function ($id) {
	// $code = App\DTRProcessed::where('id',$id)->first();
	// $processcode = $code['process_code'];

	// //DELETE PROCEESS
	// App\DTRProcessed::where('id',$id)->delete();
	// App\Employee_sala::where('process_code',$processcode)->delete();
	// App\Payroll\MC::where('process_code',$processcode)->delete();
	// App\Payroll\MCDays::where('process_code',$processcode)->delete();
	// App\Employee_hp::where('process_code',$processcode)->delete();
	// App\Employee_leave::where('process_code',$processcode)->delete();
	
	// //check leave
	// $l =  App\Request_leave::where('process_code',$processcode)->get();
	// if($l)
	// 	App\Request_leave::whereNotIn('leave_action_status',['Cancelled','Disapproved','Pending'])->where('process_code',$processcode)->update(["leave_action_status"=>"Approved", "process_code" => null]);


	// $t =  App\RequestTO::where('process_code',$processcode)->get();
	// if($t)
	// 	App\RequestTO::whereNotIn('to_status',['Cancelled','Pending'])->where('process_code',$processcode)->update(["to_status"=>"Approved", "process_code" => null]);

	// return redirect("revert-dtr");
});


Route::get('clean-dtr', function () {
	// $dtr = App\ICOSDTR::where('fldEmpDTRdate','2021-12-16')->get();

	// foreach ($dtr as $key => $value) {
	// 	echo $value->employee_name." - ".$value->fldEmpDTRdate." - ".$value->fldEmpDTRamIn;

	// 	// $check = App\Employee_icos_dtr2::where('fldEmpCode',$value->fldEmpCode)->where('fldEmpDTRdate',$value->fldEmpDTRdate)->whereIn('wfh',['Wholeday','AM','PM'])->first();
	// 	$check = App\Employee_icos_dtr2::where('fldEmpCode',$value->fldEmpCode)->where('fldEmpDTRdate',$value->fldEmpDTRdate)->whereNull('wfh')->first();

	// 	if($check)
	// 	{
	// 		App\Employee_icos_dtr2::where('id',$check['id'])
	// 		->update([
	// 					// 'fldEmpDTRpmIn' => $value->fldEmpDTRamOut,
	// 					'fldEmpDTRamOut' => $value->fldEmpDTRamOut,
	// 					'fldEmpDTRpmIn' => $value->fldEmpDTRpmIn,
	// 					'fldEmpDTRpmOut' => $value->fldEmpDTRpmOut,
	// 				]);

	// 		// echo "Doble to...Burahin ang WFH<br>";
	// 		// App\Employee_icos_dtr2::where('fldEmpCode',$value->fldEmpCode)->where('fldEmpDTRdate',$value->fldEmpDTRdate)->delete();

	// 		// $newdtr = new App\Employee_icos_dtr2;
	// 		// $newdtr->fldEmpDTRID = $value->id;
	// 		// $newdtr->fldEmpCode = $value->fldEmpCode;
	// 		// $newdtr->fldEmpDTRdate = $value->fldEmpDTRdate;
	// 		// $newdtr->fldEmpDTRamIn = $value->fldEmpDTRamIn;
	// 		// $newdtr->fldEmpDTRamOut = $value->fldEmpDTRamOut;
	// 		// $newdtr->fldEmpDTRpmIn = $value->fldEmpDTRpmIn;
	// 		// $newdtr->fldEmpDTRpmOut = $value->fldEmpDTRpmOut;
	// 		// $newdtr->save();
			
	// 	}
	// 	else
	// 	{
	// 		$newdtr = new App\Employee_icos_dtr2;
	// 		$newdtr->fldEmpDTRID = $value->id;
	// 		$newdtr->fldEmpCode = $value->fldEmpCode;
	// 		$newdtr->employee_name = $value->employee_name;
	// 		$newdtr->division = $value->division;
	// 		$newdtr->fldEmpDTRdate = $value->fldEmpDTRdate;
	// 		$newdtr->fldEmpDTRamIn = $value->fldEmpDTRamIn;
	// 		// $newdtr->fldEmpDTRamOut = $value->fldEmpDTRamOut;
	// 		// $newdtr->fldEmpDTRpmIn = $value->fldEmpDTRpmIn;
	// 		// $newdtr->fldEmpDTRpmOut = $value->fldEmpDTRpmOut;
	// 		$newdtr->save();
			
	// 	}
	// }

		// return "hello";

	// $mon = 12;
	// $yr = 2021;

	// $mon = date('F',mktime(0, 0, 0, $mon, 10));
	// $date = $mon ."-" . $yr;

	// $total = Carbon\Carbon::parse($date)->daysInMonth;
	
	// $d1 = 1;
    // $d2 = 14;

	// // $dtr = App\Employee_icos_dtr2::whereMonth('fldEmpDTRdate',12)->where('fldEmpDTRamIn','8:00:00')->where('fldEmpDTRamOut','12:00:00')->where('fldEmpDTRpmIn','13:00:00')->where('fldEmpDTRpmOut','17:00:00')->get();
	// $dtr = App\Employee_icos_dtr2::whereMonth('fldEmpDTRdate',12)->get();

	// foreach ($dtr as $key => $value) 
	// {
	// 	// App\Employee_icos_dtr2::where('id',$value->id)
	// 	// 	->update([
	// 	// 				'wfh' => 'Wholeday',
	// 	// 			]);
	// 	for($i = 1;$i <= $total; $i++)
	// 	{

	// 		if($i >= $d1 && $i <= $d2)
	// 		{
	// 			$d = $yr."-12-".$i;

	// 			$dayDesc = weekDesc(date($d));

	// 			if($dayDesc == 'Sat' || $dayDesc == 'Sun')
	// 			{
	// 				///
	// 			}
	// 			else
	// 			{
	// 				echo $value->fldEmpCode."-".$d;
	// 				$check = App\Employee_icos_dtr2::where('fldEmpCode',$value->fldEmpCode)->where('fldEmpDTRdate',$d)->first();
	// 				if(!$check)
	// 				{
	// 					$newdtr = new App\Employee_icos_dtr2;
	// 					$newdtr->fldEmpDTRID = $value->id;
	// 					$newdtr->fldEmpCode = $value->fldEmpCode;
	// 					$newdtr->fldEmpDTRdate = $d;
	// 					$newdtr->fldEmpDTRamIn = "8:00:00";
	// 					$newdtr->fldEmpDTRamOut = "12:00:00";
	// 					$newdtr->fldEmpDTRpmIn = "13:00:00";
	// 					$newdtr->fldEmpDTRpmOut = "17:00:00";
	// 					$newdtr->wfh = "Wholeday";
	// 					$newdtr->save();
	// 					$insertid = $newdtr->id;

	// 					echo "Insert...id: ".$insertid."<br>";
	// 				}
	// 				else
	// 				{
	// 					echo "<br>";
	// 				}
	// 			}
				
	// 		}
	// 	}
	// }

	
});


// Route::get('emp-pos', function () {
// 	$pos = App\PIS\EmpPos::orderBy('fldEmpPosID','DESC')->get();

// 	foreach ($pos as $key => $value) {
// 		echo $value->fldEmpCode." - ".$value->fldItemNumber." - ".$value->fldSalary." - ".$value->fldFromDate."....<br/>";
// 		$ps = App\PIS\Plantilla::where('username',$value->fldEmpCode)->count();

// 		if($ps <= 0)
// 		{
// 			$newpos = new App\PIS\Plantilla;
// 			$newpos->username = $value->fldEmpCode;
// 			$newpos->plantilla_item_number = $value->fldItemNumber;
// 			$newpos->position_id = $value->fldPosID;
// 			$newpos->plantilla_salary = $value->fldSalary;
// 			$newpos->plantilla_date_from = $value->fldFromDate;
// 			$newpos->plantilla_date_to = $value->fldFromDate;
// 			$newpos->plantilla_remarks = $value->fldRemarks;
// 			$newpos->save();
// 		}
// 	}
// });

Route::get('get-leave', function () {
	// $lv = App\Request_leave::where('leave_id',3)->whereIn('leave_action_status',['Approved','Processed','Disapproved'])->get();

	// foreach ($lv as $key => $value)
	// {
	// 	//GET PROCESSED CODE
	// 	//MONTH
	// 	$mon = date('m',strtotime($value->leave_date_from));
	// 	$yr = date('Y',strtotime($value->leave_date_from));

	// 	$proc = App\DTRProcessed::where('userid',$value->user_id)->where('dtr_mon',$mon)->where('dtr_year',$yr)->first();

	// 	if($proc)
	// 	{
	// 		echo $value->id." ~ ".$value->leave_date_from." ~ ".$value->parent." ~ ".$value->leave_id." ~ ".$value->parent_leave." ~ ".$value->parent_leave_code." ~ ".$value->leave_action_status." ~ ".$value->process_code." ------ ".$proc['process_code']."<br>";

	// 		//UPDATE PROCESS CODE
	// 		App\Request_leave::where('id',$value->id)->update(["process_code" => $proc['process_code']]);
	// 	}
	// 	else
	// 	{
	// 		echo $value->id." ~ ".$value->leave_date_from." ~ ".$value->parent." ~ ".$value->leave_id." ~ ".$value->parent_leave." ~ ".$value->parent_leave_code." ~ ".$value->leave_action_status." ~ ".$value->process_code." ------ MISSING<br>";
	// 	}

		
	// }
});


Route::get('sync-icos', function () {
	// $icos = App\DTRIcos::whereMonth('fldEmpDTRdate',12)->whereYear('fldEmpDTRdate',2021)->get();
	// foreach ($icos  as $key => $value) {
	// 	$icosnew = App\Employee_icos_dtr2::where('fldEmpDTRID',$value->id)->first();

	// 	if($icosnew)
	// 	{
	// 		echo "Date : ".$value->fldEmpDTRdate." ---- Old : ".$value->fldEmpDTRamIn."".$value->fldEmpDTRamOut."".$value->fldEmpDTRpmIn."".$value->fldEmpDTRpmOut." --- New : ".$icosnew['fldEmpDTRamIn']."".$icosnew['ldEmpDTRamOut']."".$icosnew['fldEmpDTRpmIn']."".$icosnew['fldEmpDTRpmOut']."<br>";
	// 		App\Employee_icos_dtr2::where('fldEmpDTRID',$value->id)
	// 								->update([
	// 											'fldEmpDTRamOut' => $value->fldEmpDTRamOut,
	// 											'fldEmpDTRpmIn' => $value->fldEmpDTRpmIn,
	// 											'fldEmpDTRpmOut' => $value->fldEmpDTRpmOut,
	// 											'dtr_remarks' => $value->dtr_remarks,
	// 								]);
	// 	}
	// }
});


Route::get('compute-cto', function () {
	// $cto = App\RequestOT::whereNotNull('ot_in')->whereNotNull('ot_out')->whereNotIn('ot_status',['Cancelled'])->get();
	// 	if($cto)
	// 	{
	// 		foreach ($cto as $key => $value) {

	// 			$start =  Carbon\Carbon::parse($value->ot_date." ".$value->ot_in);
	// 			$end =  Carbon\Carbon::parse($value->ot_date." ".$value->ot_out);

	// 			$totalDuration = $end->diffInMinutes($start);

	// 			$ctoearn = $totalDuration / 480;
	// 			$ctoearn = number_format($ctoearn,3);

	// 			echo "cto : ".$value->id." Employee : ".$value->employee_name." ".$totalDuration."<br/>";
	// 			App\RequestOT::where('id',$value->id)->update(['ot_min' => $totalDuration,'cto_earn' => $ctoearn]);
	// 		}
			
	// 	}

});

Route::get('test-MC', function () {

	// $mon = 2;
	// $yr = 2022;

	// $mc = App\Payroll\MC::where('payroll_mon',3)->where('payroll_yr',2022)->get();

	// foreach ($mc as $key => $value)
	// {
	// 	$user = App\User::where('id',$value->userid)->first();

	// 	if($user)
	// 	{

	// 		switch($user['employment_id'])
    //         {
    //             case 1:
    //             case 11:
    //             case 13:
    //             case 14:
    //             case 15:
	// 				//GET SALA
	// 					//TO
	// 					//GET T.O
	// 					$to_req = App\RequestTO::where('parent','YES')->where('userid',$value->userid)->where('to_status','Approved')->whereMonth('to_date_from',$mon)->whereYear('to_date_from',$yr)->get();
	// 					$to_total = 0;
	// 					if($to_req)
	// 					{
	// 						foreach ($to_req as $key => $valueto) {
								
	// 							if($valueto->to_total_day <= 1.0)
	// 							{
	// 								$to_mc_deduc = $valueto->to_total_day;
	// 							}
	// 							else
	// 							{
	// 								$to_mc_deduc = 1;
	// 							}
			
			
	// 							//GET DATE
	// 							$from = Carbon\Carbon::parse($valueto->to_date_from);
	// 							$to = Carbon\Carbon::parse($valueto->to_date_to);
	// 							$diff = 1+($from->diffInDays($to));
			
	// 							for($i = 1; $i <= $diff; $i++)
	// 							{
									
	// 								if($i == 1)
	// 								{
	// 									$dt = date('Y-m-d',strtotime($from));
	// 								}
	// 								else
	// 								{
	// 									$dt = $from->addDays(1);             
	// 								}
			
			
	// 								if(!checkIfWeekend($dt))
	// 								{
	// 									if(!checkIfHoliday($dt))
	// 									{  
	// 										if($valueto->to_perdiem == 'YES')
	// 											{
	// 												$to_total += $to_mc_deduc;
	// 											}
	// 									}
	// 								}
	// 							}
	// 						}   
	// 					}



	// 					//LEAVE
	// 					//MULTIPLE DATE
	// 					$l1_total = App\Request_leave::where('user_id',$value->userid)->whereNull('parent_leave')->whereNotNull('parent_leave_code')->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',$mon)->whereYear('leave_date_from',$yr)->count();

	// 					//SINGLE DATE
	// 					$l2_total = App\Request_leave::where('user_id',$value->userid)->where('parent','YES')->whereIn('leave_deduction',[1,0.5])->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',$mon)->whereYear('leave_date_from',$yr)->sum('leave_deduction');

	// 					$l_total = $l1_total + $l2_total;
	// 					$lv_to_total = $l_total + $to_total ;

	// 					$sa = 3150 - ($lv_to_total * 150);
    //                  $la = number_format(500 - ((500 / 22) * $lv_to_total),2);

	// 					if($user['employment_id'] == 15)
    //                           {
    //                             $sa = 0;
    //                             $la = 0;
    //                             $hp = 0;
    //                           }

	// 					echo $user['username']." SA : ".$sa." ---- LA : ".$la." ---- LEAVE : ".$l_total ." --- TO3 : ".$to_total."<br/>";

	// 					//UPDATE MC
	// 					App\Payroll\MC::where('id',$value->id)
	// 						  			->update([
	// 										  		'sa' => $sa,
	// 												'la' => $la
	// 									  ]);

	// 			break;
	// 			}
	// 	}
	// 	else
	// 	{
	// 		"Missing : ".$value->userid."<br/>";
	// 	}
	// }

});


Route::get('update-salary-mc', function () {
	// $mc = App\Payroll\MC::where('payroll_mon',1)->where('payroll_yr',2022)->get();

	// foreach ($mc as $key => $value) {
	// 		//GET SALARY
	// 		$plantilla = getPlantillaInfo($value->empcode);

	// 		//UPDATE SALARY
	// 		App\Payroll\MC::where('id',$value->id)
	// 			->update([
	// 				"salary" => $plantilla['plantilla_salary']
	// 			]);
	// }
		
});

Route::get('update-salary', function () {
	// $user = App\User::whereIn('employment_id',[1,13,14,15])->get();

	// foreach ($user as $key => $users) {
	// 	$n = getStaffInfo($users->id,'fullname');
	// 	$plantilla = getPlantillaInfo($users->username);

	// 	//SG
	// 	$salary = App\SalaryTable::where('salary_grade',$plantilla['salary_grade'])->first();

	// 	if($salary)
	// 	{
	// 		//STEP
	// 		switch ($plantilla['plantilla_step']) {
	// 			case 1:
	// 				$sal = $salary['salary_1'];
	// 			break;
	// 			case 2:
	// 				$sal = $salary['salary_2'];
	// 			break;
	// 			case 3:
	// 				$sal = $salary['salary_3'];
	// 			break;
	// 			case 4:
	// 				$sal = $salary['salary_4'];
	// 			break;
	// 			case 5:
	// 				$sal = $salary['salary_5'];
	// 			break;
	// 			case 6:
	// 				$sal = $salary['salary_6'];
	// 			break;
	// 			case 7:
	// 				$sal = $salary['salary_7'];
	// 			break;
	// 			case 8:
	// 				$sal = $salary['salary_8'];
	// 			break;
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$sal = "N/A";
	// 	}
		
	// 	//OLD
	// 	$userid = $users->id;
	// 	$username = $users->username;
	// 	$plantilla_division = $users->division;
	// 	$plantilla_item_number = $plantilla['plantilla_item_number'];
	// 	$position_id = $plantilla['position_id'];
	// 	$designation_id = $plantilla['designation_id'];
	// 	$plantilla_step = $plantilla['plantilla_step'];
	// 	$employment_id = $plantilla['employment_id'];
	// 	$plantilla_salary = $plantilla['plantilla_salary'];
	// 	$plantilla_date_from = $plantilla['plantilla_date_from'];
	// 	$plantilla_date_to = $plantilla['plantilla_date_to'];
	// 	$plantilla_special = $plantilla['plantilla_special'];



	// 	echo $n."<br/>";
	// 	echo "---OLD ITEM---<br/>";
	// 	echo "---userid : ".$plantilla."<br/>";



	// 	echo "---NEW SALARY : ".$sal."<br/>";

	// 	//CREATE NEW
	// 	$new_plantilla = new App\Plantilla;
	// 	$new_plantilla->user_id = $userid;
	// 	$new_plantilla->username = $username;
	// 	$new_plantilla->plantilla_division = $plantilla_division;
	// 	$new_plantilla->plantilla_item_number = $plantilla_item_number;
	// 	$new_plantilla->position_id = $position_id;
	// 	$new_plantilla->designation_id = $designation_id;
	// 	$new_plantilla->plantilla_step = $plantilla_step;
	// 	$new_plantilla->employment_id = $employment_id;
	// 	$new_plantilla->plantilla_salary = $sal;
	// 	$new_plantilla->plantilla_date_from = "2022-01-01";
	// 	$new_plantilla->plantilla_date_to = $plantilla_date_to;
	// 	$new_plantilla->plantilla_special = $plantilla_special;
	// 	$new_plantilla->plantilla_remarks = "increased";
	// 	$new_plantilla->save();
		

	// }
});


Route::get('update-sic', function () {
	// $user = App\User::whereIn("employment_id",[1,11,13,14,15])->get();

	// foreach ($user as $key => $users) {
	// 	//GET SIC FROM TABLE
	// 	foreach(getDeductions($users->username) AS $values)
	// 	{
	// 		//NEW SIC
	// 		$plantilla = getPlantillaInfo($users->username);

	// 		$sic = $plantilla['plantilla_salary'] * 0.09;

	// 		$diff = $sic - $values->deductAmount;

	// 		if($values->deductID == 2)
	// 		{
	// 			echo $users->username." - ".$values->deductAmount." - ".$sic." = ".$diff."<br/>";
	// 			$adj = new App\Payroll\SICAdj;
	// 			$adj->empcode = $users->username;
	// 			$adj->amount = $diff;
	// 			$adj->save();
	// 		}
				

	// 	}
	// }

	// $salary = 24136.96;

	// for ($i=1; $i <= 4 ; $i++) {
	// 	$salary = getSalaryWeek("DIA003",38150.00,$i,1);

	// 	echo $salary."<br/>";
	// }

	// $plantilla = getPlantillaInfo("CAL004");

	// return $plantilla;
		
});

Route::get('update-mc', function () {

	$sala = App\Employee_sala::where('sala_mon',4)->where('sala_year',2022)->get();

	foreach ($sala as $key => $salas) {
		//LEAVE
        //MULTIPLE DATE
        $l1_total = App\Request_leave::where('user_id',$salas->user_id)->whereNull('parent_leave')->whereNotNull('parent_leave_code')->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',3)->whereYear('leave_date_from',2022)->count();

        //SINGLE DATE
        $l2_total = App\Request_leave::where('user_id',$salas->user_id)->where('parent','YES')->whereIn('leave_deduction',[1,0.5])->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',3)->whereYear('leave_date_from',2022)->sum('leave_deduction');                     

        //T.O
        $total_to = 0;

        //MULTIPLE
        $tos = App\RequestTO::where('userid',$salas->user_id)->where('to_perdiem','YES')->whereNull('parent')->where('to_status','Approved')->whereMonth('to_date_from',3)->whereYear('to_date_from',2022)->get();
		
        foreach ($tos as $key => $toss) {
			//check if weekend
			if(!checkIfWeekend($toss['to_date_from']))
			{
				$total_to += $toss['to_total_day'];
			}
            
        }

        //SINGLE DATE
        //$tos2 = App\RequestTO::where('userid',$salas->user_id)->where('to_perdiem','YES')->whereIn('to_total_day',[1,0.5])->where('to_status','Approved')->whereMonth('to_date_from',2)->whereYear('to_date_from',2022)->sum('to_total_day');

        $l_total = $l1_total + $l2_total + $total_to;

		$sa = 2850 - ($l_total * 150);
        //$sa = $salas->sa_amt - ($l_total * 150);
        $la = 500 - ((500 / 22) * ($l1_total + $l2_total));

		if($sa < 0)
		{
			$sa = 0;
		}

		if($la < 0)
		{
			$la = 0;
		}

		//return $total_to;

		//UPDATE MC
		$mc = App\Payroll\MC::where('userid',$salas->user_id)->where('payroll_mon',4)->where('payroll_yr',2022)
							->update([
								'sa' => $sa,
								'la' => $la
							]);
	}
		
});

Route::get('update-la', function () {

	$la = App\Payroll\MC::where('payroll_mon',2)->where('payroll_yr',2022)->get();
	foreach ($la as $key => $las) {
		$lad = 454.54 - $las->la;

		// if($lad == 0)
		// {
		// 	$new_la = 500;
		// }
		// else
		// {
		// 	$new_la = 500 - $lad;
		// }

		// if($las->sa == 0)
		// {
		// 	$new_la = 0.00;
		// }
		 
		// echo $las->empcode." = ".$new_la."<br/>";

		// App\Payroll\MC::where('id',$las->id)->update(['la2' => $las->la, 'la' => $new_la]);

		// 	//LEAVE
        //MULTIPLE DATE
        $l1_total = App\Request_leave::where('user_id',$las->userid)->whereNull('parent_leave')->whereNotNull('parent_leave_code')->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',1)->whereYear('leave_date_from',2022)->count();

        //SINGLE DATE
        $l2_total = App\Request_leave::where('user_id',$las->userid)->where('parent','YES')->whereIn('leave_deduction',[1,0.5])->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',1)->whereYear('leave_date_from',2022)->sum('leave_deduction');                     
		
		$totalv = $l1_total + $l2_total;

		if($totalv == 0)
		{
			$new_la = 500;
		}
		else
		{
			$new_la = number_format(500 - ((500/22) * $totalv),2);

			if($new_la == 38.00)
			{
				$new_la = 0;
			}
		}

		

		echo $las->empcode." = ".$new_la." -- leaves -- ".$totalv." -- ".(22 * $totalv)."<br/>";

		App\Payroll\MC::where('id',$las->id)->update(['la' => $new_la]);
	}	
});

Route::get('update-comp', function () {

	$user = App\User::whereIn('employment_id',[1,11,13,14,15])->where('payroll',1)->get();

	foreach ($user as $key => $users) 
	{
		$compensation = getCompensation($users->username);
		foreach($compensation AS $lists)
								{
									if($lists->compAmount > 0)
									{
										$comp = new App\Payroll\Prevcomptbl;
										$comp->empCode = $lists->empCode;
										$comp->fldMonth = 3;
										$comp->fldYear = 2022;
										$comp->compID = $lists->compID;
										$comp->compAmount = $lists->compAmount;
										$comp->save();
									}
								}	
	}

	
});


Route::get('monitor-armrd/EzUt1cg19i', function () {
   
   $user = App\User::where('id',22)->first();

        // return request()->yr2;

        if(checkifIcos($user['id']))
        {
            $dtr = App\Employee_icos_dtr::whereYear('fldEmpDTRdate',2022)->whereMonth('fldEmpDTRdate',3)->where('user_id',$user['id'])->get();
        }
        else
        {
            $dtr = App\Employee_dtr::whereYear('fldEmpDTRdate',2022)->whereMonth('fldEmpDTRdate',3)->where('user_id',$user['id'])->get();
        }

        // return $dtr;

        $data = [
                    'date' => date('F',mktime(0, 0, 0, 3, 10)).' 2022',
                    'user' => $user['lname'] . ', ' . $user['fname'],
                    'dtr' => $dtr,
                    'userid' => $user['id'],
                ];

   return view('armrd-monitor')->with('data',$data);
});

Route::post('monitor-armrd/EzUt1cg19i', function () {
   
   $user = App\User::where('id',request()->userid)->first();

        // return request()->yr2;

        if(checkifIcos($user['id']))
        {
            $dtr = App\Employee_icos_dtr::whereYear('fldEmpDTRdate',2022)->whereMonth('fldEmpDTRdate',3)->where('user_id',$user['id'])->get();
        }
        else
        {
            $dtr = App\Employee_dtr::whereYear('fldEmpDTRdate',2022)->whereMonth('fldEmpDTRdate',3)->where('user_id',$user['id'])->get();
        }

        // return $dtr;

        $data = [
                    'date' => date('F',mktime(0, 0, 0, 4, 10)).'2022',
                    'user' => $user['lname'] . ', ' . $user['fname'],
                    'dtr' => $dtr,
                    'userid' => $user['id'],
                ];

   return view('armrd-monitor')->with('data',$data);
});

Route::get('coop-list', function () {
	$deduc = App\Payroll\Empdeduc::whereIn('SERV_CODE',["920","930"])->get();
	
	foreach ($deduc as $key => $value) {


			$salary = getPlantillaInfo($value->fldEmpCode);
			if($salary)
			{
				$fd = $salary['plantilla_salary'] * 0.02;
				echo $value->fldEmpCode." - ".$salary['plantilla_salary']." - ".$fd." - ".$value->SERV_CODE."<br>";
				//UPDATE FD
				App\Payroll\Empdeduc::where('id',$value->id)->update(['DED_AMOUNT' => $fd]);
			}
				

	}
});

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
Route::get('delete-payroll', function () {
	$path = storage_path('app/payroll/2022-04_APR');
	// File::deleteDirectory($path);
	// $response = Storage::deleteDirectory($path);
	// dd($response);

	$response = File::delete($path.'/PCAPRWK1.txt');

	$response = File::delete($path.'/PCAPRWK2.txt');

	$response = File::delete($path.'/PCAPRWK3.txt');

	$response = File::delete($path.'/PCAPRWK4.txt');
});


Route::get('get-salary', function () {
	//SAVE INFO

	// $net = 19858.88;

	// //FOR DB
	// for ($i=1; $i <= 4 ; $i++) 
	// { 
	// 	$sal = getWeekSalary('OLE002',$net,$i,1);
	// 	switch($i)
	// 	{
	// 		case 1:
	// 			$wk1 = $sal;
	// 		break;

	// 		case 2:
	// 		case 3:
	// 			$wk = $sal;
	// 		break;

	// 		case 4:
	// 			$wk4 = $sal;
	// 		break;
	// 	}
	// }

	// return $wk1;

	$timeAM = date("H:i:s",strtotime("08:00:00"));
	$timePM = date("H:i:s",strtotime("17:00:00"));

	$dt1 = Carbon\Carbon::parse('2022-03-14 '.$timeAM)->format('Y-m-d H:s:i');
	$dt2 = Carbon\Carbon::parse('2022-03-14 '.$timePM)->format('Y-m-d H:s:i');
	$to = Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $dt1);
	$from = Carbon\Carbon::createFromFormat('Y-m-d H:s:i',  $dt2);
	$diff_in_minutes = $to->diffInMinutes($from);

	return ($diff_in_minutes - 60);
});


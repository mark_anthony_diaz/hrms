TRUNCATE `employee_leaves`;

INSERT INTO employee_leaves (leave_id,empcode,leave_bal_prev,leave_bal,leave_bal_nega,created_at)
SELECT fldLeaveTypeID,fldEmpCode,fldPrevBalance,fldBalance,fldNegative,fldDate
FROM tblLeaveBalance;

UPDATE employee_leaves AS a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id = b.id;


TRUNCATE `employee_lps`;

INSERT INTO employee_lps (empcode, lp)
SELECT col1, col2
FROM lp_1

UPDATE employee_lps AS a
INNER JOIN users b ON a.empcode COLLATE utf8mb4_unicode_ci  = b.username
SET a.userid = b.id


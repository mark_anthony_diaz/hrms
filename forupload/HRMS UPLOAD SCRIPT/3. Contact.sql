/*-------PERMANENT ADDRESS INFO-------*/

TRUNCATE `employee_contacts`;

INSERT INTO employee_contacts (empcode)
	SELECT username
FROM users;

UPDATE employee_contacts a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_contacts a
INNER JOIN employeedb2.tblempcontacts b ON b.fldEmpCode = a.empcode
SET 
	a.contact_residential = b.fldResNum,
	a.contact_permanent = b.fldPerNum,
	a.contact_cellnum = b.fldCelNum;



/*-------END PERMANENT ADDRESS INFO-------*/
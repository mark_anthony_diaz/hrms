/*-------CSC INFO-------*/

TRUNCATE `employee_eligibilities`;

ALTER TABLE `employee_eligibilities` CHANGE `cse_license_date` `cse_license_date` TEXT NULL DEFAULT NULL;

UPDATE employee_eligibilities SET approved = 1;

INSERT INTO employee_eligibilities (empcode,cse_title,cse_rating,cse_date,cse_place,cse_license_num,cse_license_date)
SELECT fldEmpCode,fldCService,fldRating,fldDateExam,fldPlaceExam,fldLicenseNum,fldLicenseDate
FROM  employeedb2.tblEmpCSE;


UPDATE employee_eligibilities a
INNER JOIN employeedb2.tblEmpCSE b ON b.fldEmpCode = a.empcode
SET 
	a.deleted_at = if(b.fldDelete = 0,NULL,b.fldDeleteDate);

UPDATE employee_eligibilities SET cse_license_date = IF(cse_license_date = '0000-00-00',NULL,cse_license_date);

ALTER TABLE `employee_eligibilities` CHANGE `cse_license_date` `cse_license_date` DATE NULL DEFAULT NULL;

UPDATE employee_eligibilities a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

/*-------END CSC INFO-------*/
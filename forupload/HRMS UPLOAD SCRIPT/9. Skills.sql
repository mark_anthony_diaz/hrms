/*-------SKILLS INFO-------*/

TRUNCATE `employee_skills`;

INSERT INTO employee_skills (empcode,skill_desc)
SELECT fldEmpCode,fldSkills
FROM  employeedb2.tblEmpSkills;


UPDATE employee_skills a
INNER JOIN employeedb2.tblEmpTrainProg b ON b.fldEmpCode = a.empcode
SET
	a.deleted_at = if(b.fldDelete = 0,NULL,b.fldDeleteDate);


UPDATE employee_skills a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;


/*-------END SKILLS INFO-------*/
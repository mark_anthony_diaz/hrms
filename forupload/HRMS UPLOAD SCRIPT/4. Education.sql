/*-------EDUCATION INFO-------*/

TRUNCATE `employee_educations`;

INSERT INTO employee_educations (empcode,educ_level,educ_school,educ_course,educ_highest,educ_date_from,educ_date_to,educ_awards)
SELECT fldEmpCode,fldEducLevelID,fldSchool,fldCourse,fldHighest,fldFromDate,fldToDate,fldAwards
FROM  employeedb2.tblEmpEducBack;


UPDATE employee_educations a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_educations a
INNER JOIN employeedb2.tblEmpEducBack b ON b.fldEmpCode = a.empcode
SET
	a.deleted_at = if(b.fldDelete = 0,NULL,b.fldDeleteDate);

	UPDATE employee_educations SET approved = 1;


UPDATE employee_educations a
INNER JOIN employeedb2.tbleducationlevel b ON b.fldEducLevelID = a.educ_level
SET
	a.educ_level_desc = b.fldEducLevel;

/*-------END PEDUCATION INFO-------*/
/*-------PERMANENT ADDRESS INFO-------*/

TRUNCATE `employee_address_permanents`;

INSERT INTO employee_address_permanents (empcode)
SELECT username
FROM  users;

UPDATE employee_address_permanents a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_address_permanents a
INNER JOIN employeedb2.tblPermanentAddress b ON b.fldEmpCode = a.empcode
SET 
	a.permanent_add_street = b.fldStreet,
	a.permanent_add_subd = b.fldSubdivision,
	a.permanent_add_brgy = b.fldBarangay,
	a.permanent_add_no = b.fldNumber,
	a.permanent_add_mun = b.fldMunicipality,
	a.permanent_add_prov = b.fldProvince,
	a.permanent_add_zipcode = b.fldZipCode;


/*-------END PERMANENT ADDRESS INFO-------*/


/*-------RESIDENTIAL ADDRESS INFO-------*/

TRUNCATE `employee_address_residentials`;

INSERT INTO employee_address_residentials (empcode)
SELECT username
FROM  users;

UPDATE employee_address_residentials a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_address_residentials a
INNER JOIN employeedb2.tblPermanentAddress b ON b.fldEmpCode = a.empcode
SET 
	a.residential_add_street = b.fldStreet,
	a.residential_add_subd = b.fldSubdivision,
	a.residential_add_brgy = b.fldBarangay,
	a.residential_add_no = b.fldNumber,
	a.residential_add_mun = b.fldMunicipality,
	a.residential_add_prov = b.fldProvince,
	a.residential_add_zipcode = b.fldZipCode;


/*-------END RESIDENTIAL ADDRESS INFO-------*/
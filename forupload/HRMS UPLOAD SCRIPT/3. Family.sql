/*-------PERMANENT ADDRESS INFO-------*/

TRUNCATE `employee_families`;

INSERT INTO employee_families (empcode)
SELECT username
FROM users;

UPDATE employee_families a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_families a
INNER JOIN employeedb2.tblEmpFather b ON b.fldEmpCode = a.empcode
SET 
	a.fam_father_lname = b.fldFLName,
	a.fam_father_fname = b.fldFFName,
	a.fam_father_mname = b.fldFMName,
	a.fam_father_exname = b.fldNameExtension;


UPDATE employee_families a
INNER JOIN employeedb2.tblEmpMother b ON b.fldEmpCode = a.empcode
SET 
	a.fam_mother_maiden = b.fldMaidenName,
	a.fam_mother_lname = b.fldMLName,
	a.fam_mother_fname = b.fldMFName,
	a.fam_mother_mname = b.fldMMName;


UPDATE employee_families a
INNER JOIN employeedb2.tblEmpSpouseInfo b ON b.fldEmpCode = a.empcode
SET 
	a.fam_spouse_lname = b.fldSLName,
	a.fam_spouse_fname = b.fldSFName,
	a.fam_spouse_mname = b.fldSMName,
	a.fam_spouse_exname = b.fldNameExtension,
	a.fam_spouse_occ = b.fldOccupation,
	a.fam_spouse_emp = b.fldEmployer,
	a.fam_spouse_emp_add = b.fldAddress,
	a.fam_spouse_tel = b.fldTelNo;


/*-------END PERMANENT ADDRESS INFO-------*/
/*-------ORGANIZATION INFO-------*/

TRUNCATE `employee_organizations`;

INSERT INTO employee_organizations (empcode,org_name,org_add,org_date_from,org_date_to,org_hours,org_position,org_nature)
SELECT fldEmpCode,fldOrgName,fldOrgAddress,fldFromDate,fldToDate,fldNumHours,fldPosition,fldNatureWork
FROM  employeedb2.tblEmpOrg;

UPDATE employee_organizations a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_organizations a
INNER JOIN employeedb2.tblEmpOrg b ON b.fldEmpCode = a.empcode
SET 
	a.deleted_at = if(b.fldDelete = 0,NULL,b.fldDeleteDate);

/*-------END ORGANIZATION INFO-------*/
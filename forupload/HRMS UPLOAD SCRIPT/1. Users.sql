TRUNCATE `users`;

INSERT INTO users (username,lname,fname,mname,exname,fldservice)
SELECT fldEmpCode,fldEmpLName,fldEmpFName,fldEmpMName,fldEmpEName,fldService
FROM  employeedb2.tblEmployees;

UPDATE users a
INNER JOIN employeedb2.tblEmpInfo b ON b.fldEmpCode = a.username
SET 
	a.birthdate  = b.fldDateBirth,
	a.birthplace = b.fldPlaceBirth,
	a.sex = if(b.fldSexID = 1,'Male','Female');


/*-------BASIC INFO-------*/

TRUNCATE `employee_basicinfos`;

INSERT INTO employee_basicinfos (empcode)
SELECT username
FROM  users;

UPDATE employee_basicinfos a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_basicinfos a
INNER JOIN employeedb2.tblEmpInfo b ON b.fldEmpCode = a.empcode
SET 
	a.basicinfo_placeofbirth = b.fldPlaceBirth,
	a.basicinfo_sex = if(b.fldSexID = 1,'Male','Female'),
	a.basicinfo_civilstatus = b.fldCivilStatusID,
	a.basicinfo_citizenship = 'Filipino',
	a.basicinfo_citizentype = if(b.fldCitizenshipTypeID = 1,'By birth','By naturalization'),
	a.basicinfo_height = b.fldHeight,
	a.basicinfo_weight = b.fldWeight,
	a.basicinfo_bloodtype = b.fldBloodTypeID;

UPDATE employee_basicinfos a
INNER JOIN employeedb2.tblCivilStatus b ON a.basicinfo_civilstatus = b.fldCivilStatusID
SET a.basicinfo_civilstatus  = b.fldCivilStatus;


UPDATE employee_basicinfos a
INNER JOIN employeedb2.tblBloodType b ON a.basicinfo_bloodtype = b.fldBloodTypeID
SET a.basicinfo_bloodtype  = b.fldBloodType;

/*-------END BASIC INFO-------*/



/*-------ADDITIONAL INFO-------*/

TRUNCATE `employee_addinfos`;

INSERT INTO employee_addinfos (empcode)
SELECT username
FROM  users;

UPDATE employee_addinfos a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;

UPDATE employee_addinfos a
INNER JOIN employeedb2.tblIDNumbers b ON b.fldEmpCode = a.empcode
SET 
	a.addinfo_pagibig = b.fldPagibig,
	a.addinfo_philhealth = b.fldPhilHealth,
	a.addinfo_sss = b.fldSSS,
	a.addinfo_tin = b.fldTIN,
	a.addinfo_gsis_id = b.fldGSIS,
	a.addinfo_ctc = b.fldCTCNo,
	a.addinfo_ctc_date = b.fldDateIssued,
	a.addinfo_ctc_place = b.fldPlaceIssued,
	a.addinfo_gov = b.fldGovtIssuedID,
	a.addinfo_gov_id = b.fldGovtIssuedIDNumber,
	a.addinfo_gov_place_date = b.fldGovtIssuedIDPlaceIssued;


UPDATE employee_addinfos a
INNER JOIN employeedb2.tblotheridnumbers b ON b.fldEmpCode = a.empcode
SET 
	a.addinfo_gsis_bp = b.gsisbpnumber;

/*-------END ADDITIONAL INFO-------*/

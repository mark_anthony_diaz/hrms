/*-------WORK INFO-------*/

TRUNCATE `employee_works`;

ALTER TABLE `employee_works` CHANGE `workexp_date_to` `workexp_date_to` TEXT NULL DEFAULT NULL;

ALTER TABLE `employee_works` CHANGE `workexp_date_from` `workexp_date_from` TEXT NULL DEFAULT NULL;



INSERT INTO employee_works (username,workexp_date_from,workexp_date_to,workexp_title,workexp_company,workexp_salary,workexp_salary_grade,workexp_empstatus,workexp_gov,workexp_admin)
SELECT fldEmpCode,fldFromDate,fldToDate,fldPositionTitle,fldCompany,fldSalary,fldSalaryGrade,fldStatusApp,fldGovService,fldAdminInput
FROM  employeedb2.tblEmpWorkExp;

UPDATE employee_works a
INNER JOIN employeedb2.tblEmpWorkExp b ON b.fldEmpCode = a.username
SET
	a.deleted_at = if(b.fldDelete = 0,NULL,b.fldDeleteDate);
	

UPDATE employee_works SET workexp_date_to = IF(workexp_date_to = '0000-00-00',NULL,workexp_date_to);

ALTER TABLE `employee_works` CHANGE `workexp_date_to` `workexp_date_to` DATE NULL DEFAULT NULL;

UPDATE employee_works SET workexp_date_from = IF(workexp_date_from = '0000-00-00',NULL,workexp_date_from);

ALTER TABLE `employee_works` CHANGE `workexp_date_from` `workexp_date_from` DATE NULL DEFAULT NULL;

UPDATE employee_works SET workexp_gov_desc = IF(workexp_gov = 2,"Government","Private");

UPDATE employee_works a
INNER JOIN users b ON a.username = b.username
SET a.user_id  = b.id;


/*-------END WORK INFO-------*/
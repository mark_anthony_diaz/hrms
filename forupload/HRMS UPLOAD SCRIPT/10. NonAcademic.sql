TRUNCATE `employee_nonacademics`;

-- ALTER TABLE `employee_nonacademics` ADD `user_id` INT NULL DEFAULT NULL AFTER `updated_at`, ADD `empcode` VARCHAR(50) NULL DEFAULT NULL AFTER `user_id`, ADD `academic_desc` TEXT NULL DEFAULT NULL AFTER `empcode`, ADD `deleted_at` TIMESTAMP NULL DEFAULT NULL AFTER `academic_desc`;

INSERT INTO employee_nonacademics (empcode,academic_desc)
SELECT fldEmpCode,fldNonAcademic
FROM  employeedb2.tblEmpNonAcademic;

UPDATE employee_nonacademics a
INNER JOIN employeedb2.tblEmpNonAcademic b ON b.fldEmpCode = a.empcode
SET
	a.deleted_at = if(b.fldDelete = 0,NULL,b.fldDeleteDate);

UPDATE employee_nonacademics a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id  = b.id;


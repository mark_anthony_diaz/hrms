/*-------PLANTILLA INFO-------*/

-- UPDATE EMLOYEEDB2
ALTER TABLE employeedb2.tblemppos CHANGE `fldFromDate` `fldFromDate` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL, CHANGE `fldToDate` `fldToDate` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL;
UPDATE employeedb2.tblemppos SET fldFromDate = NULL WHERE fldFromDate = '0000-00-00';
UPDATE employeedb2.tblemppos SET fldToDate = NULL WHERE fldToDate = '0000-00-00';

TRUNCATE `plantillas`;

INSERT INTO plantillas (username,plantilla_division,plantilla_item_number,position_id,plantilla_step,employment_id,plantilla_salary,plantilla_date_from,plantilla_date_to,plantilla_special,plantilla_remarks)
SELECT fldEmpCode,fldDivID,fldItemNumber,fldPosID,fldSIID,fldEmployID,fldSalary,fldFromDate,fldToDate,fldSpecial,fldRemarks
FROM  employeedb2.tblemppos;


UPDATE plantillas a
INNER JOIN users b ON a.username = b.username
SET a.user_id  = b.id;

CREATE OR REPLACE VIEW view_employee_position AS
SELECT
		a.*,
		b.position_desc,
		b.position_abbr,
		b.stepincrement_id AS salary_grade,
		c.employment_desc,
		(SELECT fldservice FROM users WHERE username = a.username) AS first_emp_date
FROM
		plantillas AS a
LEFT JOIN
		positions AS b ON a.position_id = b.position_id
LEFT JOIN
		employments AS c ON a.employment_id = c.employment_id

/*-------END PLANTILLA INFO-------*/
INSERT INTO `d_t_r_processeds` (`userid`, `empcode`,`dtr_mon`, `dtr_year`,`created_at`,vl_leave,vl_earn,vl_bal,sl_leave,sl_earn,sl_bal)
SELECT a.user_id, a.fldEmpCode, a.fldMonth,a.fldYear,2021,NOW(),a.fldVLUsedLeave,a.fldVLEarned,a.fldVLBalance,a.fldSLUsed,a.fldSLEarned,a.fldSLBalance
FROM   tblsummary AS a


UPDATE tblsummary AS a
INNER JOIN users b ON a.fldEmpCode = b.username
SET a.user_id = b.id


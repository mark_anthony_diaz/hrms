TRUNCATE `request_o_t_s`;

INSERT INTO request_o_t_s (empcode,division,ot_date,ot_purpose,ot_output,created_at)
SELECT fldEmpCode,fldDivCode,fldFromDate,fldReason,fldOutput,fldDateFiled
FROM camsonline2.tblOtRequest;

UPDATE request_o_t_s AS a
INNER JOIN users b ON a.empcode = b.username
SET a.user_id = b.id,


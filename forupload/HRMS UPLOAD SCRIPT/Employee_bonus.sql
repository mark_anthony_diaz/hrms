INSERT INTO employee_bonuses (user_id,empcode,bonus_id,bonus_amt,bonus_year)
SELECT 
	id,username,1,
	(SELECT plantilla_salary FROM view_employee_position WHERE username = users.username AND plantilla_date_to IS NULL LIMIT 1),
	2020
FROM users 
WHERE
employment_id NOT IN(10,8);
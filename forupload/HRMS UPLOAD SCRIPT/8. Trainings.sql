/*-------TRAINING INFO-------*/

TRUNCATE `employee_trainings`;

INSERT INTO employee_trainings (username,training_title,training_date_from,training_date_to,training_hours,training_ld,training_conducted_by)
SELECT fldEmpCode,fldTitle,fldFromDate,fldToDate,fldNumHours,fldLDType,fldConductedBy
FROM  employeedb2.tblEmpTrainProg;

-- ALTER TABLE `employee_works` CHANGE `workexp_date_to` `workexp_date_to` TEXT NULL DEFAULT NULL;

UPDATE employee_trainings a
INNER JOIN employeedb2.tblEmpTrainProg b ON b.fldEmpCode = a.username
SET
	a.training_type = "Free",
	a.deleted_at = if(b.fldDelete = 0,NULL,b.fldDeleteDate);

-- UPDATE employee_trainings SET workexp_date_to = IF(workexp_date_to = '0000-00-00',NULL,workexp_date_to);

-- ALTER TABLE `employee_trainings` CHANGE `workexp_date_to` `workexp_date_to` DATE NULL DEFAULT NULL;

UPDATE employee_trainings SET approved = 1;

UPDATE employee_trainings a
INNER JOIN users b ON a.username = b.username
SET a.user_id  = b.id;


UPDATE employee_trainings a
INNER JOIN view_emp_div b ON b.username = a.username
SET 
	a.division_id = b.division

/*-------END TRAINING INFO-------*/
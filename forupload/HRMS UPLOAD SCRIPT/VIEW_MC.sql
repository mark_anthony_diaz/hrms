CREATE OR REPLACE VIEW view_mcs AS
SELECT
	a.*,
	b.lname,b.fname,b.mname,b.exname,b.employment_id,
	c.addinfo_atm
FROM
	employee_mcs AS a
LEFT JOIN
	users AS b ON a.empcode = b.username
LEFT JOIN
	employee_addinfos AS c ON a.empcode = c.empcode 
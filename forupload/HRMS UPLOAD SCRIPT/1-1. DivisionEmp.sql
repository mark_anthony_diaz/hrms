-- ADD DIVISION
ALTER TABLE employeedb2.tblEmpDivDes CHANGE `fldEmpCode` `fldEmpCode` CHAR(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, CHANGE `fldFromDate` `fldFromDate` TEXT NOT NULL, CHANGE `fldToDate` `fldToDate` TEXT NOT NULL;

CREATE OR REPLACE VIEW view_emp_div AS
SELECT
	a.username,
	(SELECT fldDivID FROM employeedb2.tblEmpDivDes WHERE fldEmpCode = a.username ORDER BY fldToDate ASC LIMIT 1) AS division
FROM
	users AS a;

UPDATE users a
INNER JOIN view_emp_div b ON b.username = a.username
SET 
	a.division = b.division;
-- DIVISION
TRUNCATE `applicants`;
TRUNCATE `applicant_position_applies`;
TRUNCATE `employee_training_temps`;
TRUNCATE `h_r_d_hrdcs`;
TRUNCATE `h_r_d_plans`;
TRUNCATE `h_r_d_plan_degrees`;
TRUNCATE `h_r_d_plan_divisions`;
TRUNCATE `h_r_d_plan_non_degrees`;
TRUNCATE `h_r_d_plan_non_degree_areas`;
TRUNCATE `h_r_d_plan_staffs`;
TRUNCATE `h_r_d_remarks`;
TRUNCATE `invitations`;
TRUNCATE `link_to_applicants`;
TRUNCATE `performance_dpcrs`;
TRUNCATE `performance_ipcrs`;
TRUNCATE `performance_ipcr_staffs`;
TRUNCATE `recruitment_file_histories`;
TRUNCATE `recruitment_histories`;
TRUNCATE `recruitment_letter_histories`;
TRUNCATE `request_for_hirings`;
TRUNCATE `vacant_plantillas`;

/*--CLEAR--*/
DELETE FROM users WHERE id > 356;



/*CLEAR FINAL PROCESSS*/
DELETE FROM employee_hps WHERE process_code IN ('bkSkrYPCEhRTmLwYMPVN','U100ZWHg6vp8Pn0UIxW3','KG1l3LL0JGnDehUY8x0e','USnqPIYiFxp3us6mmxJl','YW79ALj5biYptr9E86ks','hSTYWs9xxNWPWRd30cth','HUdpjeW7LN6cM0o3VPxi','pXnGSuksD4hYYBjPI39n','X9PB8oywGK2jVQ1CWLt3','nFC0JVErEfGSHymKQdUy');
DELETE FROM employee_salas WHERE process_code IN ('bkSkrYPCEhRTmLwYMPVN','U100ZWHg6vp8Pn0UIxW3','KG1l3LL0JGnDehUY8x0e','USnqPIYiFxp3us6mmxJl','YW79ALj5biYptr9E86ks','hSTYWs9xxNWPWRd30cth','HUdpjeW7LN6cM0o3VPxi','pXnGSuksD4hYYBjPI39n','X9PB8oywGK2jVQ1CWLt3','nFC0JVErEfGSHymKQdUy');
DELETE FROM employee_leaves WHERE process_code IN ('bkSkrYPCEhRTmLwYMPVN','U100ZWHg6vp8Pn0UIxW3','KG1l3LL0JGnDehUY8x0e','USnqPIYiFxp3us6mmxJl','YW79ALj5biYptr9E86ks','hSTYWs9xxNWPWRd30cth','HUdpjeW7LN6cM0o3VPxi','pXnGSuksD4hYYBjPI39n','X9PB8oywGK2jVQ1CWLt3','nFC0JVErEfGSHymKQdUy');
DELETE FROM d_t_r_processeds WHERE process_code IN ('bkSkrYPCEhRTmLwYMPVN','U100ZWHg6vp8Pn0UIxW3','KG1l3LL0JGnDehUY8x0e','USnqPIYiFxp3us6mmxJl','YW79ALj5biYptr9E86ks','hSTYWs9xxNWPWRd30cth','HUdpjeW7LN6cM0o3VPxi','pXnGSuksD4hYYBjPI39n','X9PB8oywGK2jVQ1CWLt3','nFC0JVErEfGSHymKQdUy');



<?php


$from = Carbon::parse($duration[0]);
$from_orig = Carbon::parse($duration[0]);
$to = Carbon::parse($duration[1]);

$diff = 1+($from->diffInDays($to));

//CHECK HAS LEAVE BALANCE, WAG LANG VL AT SL
$bal = getLeaves(Auth::user()->id,$leaveid);

$pending = getPending($leaveid);
$projected = $bal - $pending;

$rem_bal = $projected - $diff;

if($rem_bal > 0 || $no_nega == false)
{
    //FOR MULTIPLE DATES, MAHIRAP KASI I-CHECK INDIVIDUAL DATES TO CHECK IF EXISTING YUNG LEAVE DATE
    $code = randomCode(15);

    //GET UPDATED LEAVE
    $leaves = getLeaves(Auth::user()->id,$leaveid);
    $leaves_loop = $leaves;

    //CHECK IF SINGLE DATE
    if($diff == 1)
    {
        // $lwop = countDiffLeave($leaves,request()->leave_time);
        //CHECK LEAVE TYPE
        $lwop = null;

        if($leaveid == 1)
            {
                if($rem_bal < 0)
                    $lwop = "Vacation Leave";
            }
            elseif($leaveid == 2)
            {
                //CHECK IF HAS A VL
                $bal = getLeaves(Auth::user()->id,1);

                if($bal <= 0)
                    $lwop = "Sick Leave";
                else
                    $leaveid = 1;
            }

        $this->addLeave($leaveid,date('Y-m-d',strtotime($from)),date('Y-m-d',strtotime($to)),request()->leave_time,null,$code,$code,'YES',$lwop,null,request()->vl_select,request()->vl_select_specify,request()->sl_select,request()->sl_select_specify,request()->wfh_reason,request()->wfh_output);
    }
    else
    {    
        $totaldays = 0;

        $dt_to = date('Y-m-d',strtotime($to));
        
        for($i = 1; $i <= $diff; $i++)
        {

            if($i == 1)
                {
                    $dt = date('Y-m-d',strtotime($from));
                }
                else
                {
                        $dt_main = $from->addDays(1);
                        $dt = $dt_main;
                    
                }

                if(!$this->checkIfWeekend($dt))
                    {
                        if(!checkIfHoliday($dt))
                        {
                            if(!checkIfHasLeave($dt))
                            {
                                // $lwp = 'NO';
                                // if($leaves_loop <= 0)
                                //     {
                                //        $lwp = 'YES';
                                //     }
                                $lwop = null;
                                $lwop_parent = null;
                                if($leaveid == 1)
                                    {
                                        if($leaves_loop < 0)
                                        {
                                            $lwop = "VL";
                                            break;
                                        }
                                    }
                                elseif($leaveid == 2)
                                    { 
                                        if($leaves_loop < 0)
                                            {
                                                $lwop = "SL";
                                                break;
                                            }
                                                
                                    }
                                
                                $this->addLeave($leaveid,date('Y-m-d',strtotime($dt)),null,'multiple',1,null,$code,null,$lwop,$leaves_loop,null,request()->vl_select,request()->vl_select_specify,request()->sl_select,request()->sl_select_specify,request()->wfh_reason,request()->wfh_output);

                                $dt_to = date('Y-m-d',strtotime($dt));
                                
                                $totaldays++;
                                $leaves_loop--;
                            }
                        }

                    }
            
        }
        // $lwop = countDiffLeave($leaves,$totaldays);

        $this->addLeave($leaveid,date('Y-m-d',strtotime($from_orig)),$dt_to,'multiple',$totaldays,$code,$code,'YES',$lwop_parent,null,request()->vl_select,request()->vl_select_specify,request()->sl_select,request()->sl_select_specify,request()->wfh_reason,request()->wfh_output);


        //NEW DATE DIFFERENCE
        $new_from = Carbon::parse($dt_to);
        $new_from = $new_from->addDays(1);
        $orig_to = Carbon::parse(date('Y-m-d',strtotime($to)));

        $new_diff = 1+($new_from->diffInDays($orig_to));

        //ADD NEW LEAVE REQUEST BUT LWOP
        //VL
        if($lwop == 'VL')
        {
            $code = randomCode(15);
            for($i = 1; $i <= $new_diff; $i++)
            {
                if($i == 1)
                {
                    $dt = date('Y-m-d',strtotime($from));
                }
                else
                {
                        $dt_main = $from->addDays(1);
                        $dt = $dt_main;
                    
                }

                if(!$this->checkIfWeekend($dt))
                    {
                        if(!checkIfHoliday($dt))
                        {
                            if(!checkIfHasLeave($dt))
                            {
                                $this->addLeave($leaveid,date('Y-m-d',strtotime($dt)),null,'multiple',1,null,$code,null,'Vacation Leave',$leaves_loop,null,request()->vl_select,request()->vl_select_specify,request()->sl_select,request()->sl_select_specify,request()->wfh_reason,request()->wfh_output);
                            }
                        }

                    }
            }
            $this->addLeave(1,date('Y-m-d',strtotime($new_from)),$orig_to,'multiple',$new_diff,$code,$code,'YES','YES',null,request()->vl_select,request()->vl_select_specify,request()->sl_select,request()->sl_select_specify,request()->wfh_reason,request()->wfh_output);
        }

        //SL
        else
        {

            //CHECK IF MAY VL PA
            //CHECK HAS LEAVE BALANCE, WAG LANG VL AT SL
            $bal = getLeaves(Auth::user()->id,1);
            $pending = getPending(1);
            $projected = $bal - $pending;
            $rem_bal = $projected - $new_diff;

            if($rem_bal < 0)
            {

            }
            else
            {
                $code = randomCode(15);
                for($i = 1; $i <= $new_diff; $i++)
                {
                    if($i == 1)
                    {
                        $dt = date('Y-m-d',strtotime($from));
                    }
                    else
                    {
                            $dt_main = $from->addDays(1);
                            $dt = $dt_main;
                        
                    }

                    if(!$this->checkIfWeekend($dt))
                        {
                            if(!checkIfHoliday($dt))
                            {
                                if(!checkIfHasLeave($dt))
                                {
                                    $this->addLeave(2,date('Y-m-d',strtotime($dt)),null,'multiple',1,null,$code,null,'Sick Leave',$leaves_loop,null,request()->vl_select,request()->vl_select_specify,request()->sl_select,request()->sl_select_specify,request()->wfh_reason,request()->wfh_output);
                                }
                            }

                        }
                }
                $this->addLeave(2,date('Y-m-d',strtotime($new_from)),$orig_to,'multiple',$new_diff,$code,$code,'YES','YES',null,request()->vl_select,request()->vl_select_specify,request()->sl_select,request()->sl_select_specify,request()->wfh_reason,request()->wfh_output);
            }
        }

    }

    return redirect('/');
} 
else
{

    return view('error-message')->with('error_message','Not enough leave balance..');
}
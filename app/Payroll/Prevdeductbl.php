<?php

namespace App\Payroll;

use Illuminate\Database\Eloquent\Model;

class Prevdeductbl extends Model
{
    protected $connection = 'payroll';
    protected $table = 'tblprevdeduct';
}

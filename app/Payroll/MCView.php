<?php

namespace App\Payroll;

use Illuminate\Database\Eloquent\Model;

class MCView extends Model
{
    protected $table = 'view_mcs';
}

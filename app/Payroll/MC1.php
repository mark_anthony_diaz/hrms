<?php

namespace App\Payroll;

use Illuminate\Database\Eloquent\Model;

class MC1 extends Model
{
    protected $connection = 'payroll';
    public $timestamps = false;
    protected $table = 'table_45';
}

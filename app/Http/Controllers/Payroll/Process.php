<?php

namespace App\Http\Controllers\Payroll;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemManager;
use File;
use App;
use App\Imports\SalaryImport;

use Maatwebsite\Excel\Facades\Excel;

class Process extends Controller
{
    // private $info = "";

    public function __construct()
    {
        $this->middleware('auth');
        
    }

    private function getInfo($id)
    {
        $emp = 
        $emp = $this->info->where('user_id',$id)->orderBy('plantilla_date_to')->first();
        return $emp['plantilla_salary'];
    }

    public function index()
    {
        $data = 
                [
                    "nav" => nav("payrollprocess"),
                ];
        return view('payroll.process')->with("data",$data);
    }

    public function create()
    {

        //ACTIVE USER
        $emp = App\View_user::whereNotNull('payroll')->get();

        //GET MONTH YEAR
        $dt = explode("-",request()->path);
        $p_year = $dt[0];
        $p_mon = $dt[1];

        foreach ($emp as $ky => $vl) 
        {
            //SAVE INFO
            $plantilla = getPlantillaInfo($vl->username);
            $sl = $plantilla['plantilla_salary'];

            $ra = 0;
            $ta = 0;
            $bir = 0;
            $sic = 0;
            $gsis = 0;
            $hmdf = 0;
            $ph = 0;
            $others = 0;

            $total_deduc = 0;

            //SAVE MANDATORY
            $manda = getDeductions($vl->username);
            foreach ($manda as $km => $vm) 
                {
                    if($vm->deductAmount > 0)
                    {
                        $d = new App\Payroll\Prevmandatbl;
                        $d->empCode = $vl->username;
                        $d->fldMonth = $p_mon;
                        $d->fldYear = $p_year;
                        $d->deductID = $vm->deductID; 
                        $d->deductAmount = $vm->deductAmount;
                        $d->save();

                        switch($d->deductID)
                        {
                            case 1:
                                $bir = $vm->deductAmount;
                                $total_deduc += $vm->deductAmount;
                            break;

                            case 2:
                                $sic = $sl * 0.09;
                                $total_deduc += $sic;
                            break;

                            case 3:
                                $ph = $vm->deductAmount;
                                $total_deduc += $vm->deductAmount;
                            break;
                            
                            case 4:
                                $hmdf = $vm->deductAmount;
                                $total_deduc += $vm->deductAmount;
                            break;
                        }
                    }
                }
            
                
            
            //SAVE LOANS/OTHERS
            $loans = getPersonalLoans($vl->username);
            $other_d = 0;
            
                    
            foreach ($loans as $kd => $vd) 
                {
                    if($vd->DED_AMOUNT > 0)
                    {
                        $other_d += $vd->DED_AMOUNT;
                        $total_deduc += $vd->DED_AMOUNT;

                        $d = new App\Payroll\Prevdeductbl;
                        $d->fldEmpCode = $vl->username;
                        $d->fldMonth = $p_mon;
                        $d->fldYear = $p_year;
                        $d->ORG_CODE = $vd->ORG_CODE; 
                        $d->SERV_CODE = $vd->SERV_CODE; 
                        $d->SERV_NO = $vd->SERV_NO; 
                        $d->DED_AMOUNT = $vd->DED_AMOUNT;
                        $d->save();
                        
                    }
                   
                }


            //SAVE COMPENSATION
            $comps = getCompensation($vl->username);
                    
            foreach ($comps as $kc => $vc) 
                {
                    if($vc->compAmount > 0)
                    {
                        $d = new App\Payroll\Prevcomptbl;
                        $d->empCode = $vl->username;
                        $d->compID = $vc->compID;
                        $d->compAmount = $vc->compAmount;
                        $d->fldMonth = $p_mon;
                        $d->fldYear = $p_year;
                        $d->save();
                    }
                   
                }


            
            $wk = 0;
            $wk1 = 0;
            $wk4 = 0;

            $net = ($sl + 2000) - $total_deduc;

            //FOR DB
            for ($i=1; $i <= 4 ; $i++) 
            { 
                $sal = getWeekSalary($vl->username,$net,$i,1);
                switch($i)
                {
                    case 1:
                        $wk1 = $sal;
                    break;

                    case 2:
                    case 3:
                        $wk = $sal;
                    break;

                    case 4:
                        $wk4 = $sal;
                    break;
                }
            }
            

             $info = new App\Payroll\PrevInfo;
             $info->fldEmpCode = $vl->username;
             $info->fldMonth = $p_mon;
             $info->fldYear = $p_year;
             $info->M_BASIC = $sl;
             $info->M_PERA = 2000;
             $info->M_REPN = $ra;
             $info->M_TRANS = $ta;
             $info->SIC = $sic;
             $info->BIR = $bir;
             $info->HDMF = $hmdf;
             $info->OTH_DED = $other_d;
             $info->AMOUNT1 = $wk1;
             $info->AMOUNT2 = $wk;
             $info->AMOUNT3 = $wk;
             $info->AMOUNT4 = $wk4;
             $info->netSalary = $net;
             $info->save();
             
            
        }

        //CREATE FILE
        //MAX CHARACTER
        $max = 73;
        $max2 = 23;

        //CONST FOR LANDBANK
        $atm = "1890000";

        $folder = request()->path;

        //Storage::disk('payroll')->deleteDirectory('2022-03_MAR');

        Storage::disk('payroll')->makeDirectory($folder);

        $fsMgr = new FilesystemManager(app());
        // local disk
        $localDisk = $fsMgr->createLocalDriver(['root' => storage_path('app/payroll/'.$folder)]);

            for ($i=1; $i <= 4 ; $i++) 
            { 

                $txt = "";

                foreach ($emp as $ky => $vl) 
                {
                        //SAVE INFO
                        $plantilla = getPlantillaInfo($vl->username);
                        $sl = $plantilla['plantilla_salary'];

                        $total_deduc = 0;

                        //SAVE MANDATORY
                        $manda = getDeductions($vl->username);
                        foreach ($manda as $kd => $vd) 
                        {
                            if($vd->deductID == 2)
                            {
                                $d_sic = $sl * 0.09;
                                $total_deduc += $d_sic;
                            }
                            else
                            {
                                $total_deduc += $vd->deductAmount;
                            }
                        }   
                        
                        //SAVE LOANS/OTHERS
                        $loans = getPersonalLoans($vl->username);
                        $other_d = 0;
                                
                        foreach ($loans as $kd => $vd) 
                            {
                                if($vd->DED_AMOUNT > 0)
                                {
                                    $other_d += $vd->DED_AMOUNT;
                                    $total_deduc += $vd->DED_AMOUNT;
                                }
                            
                            }


                        $net = ($sl + 2000) - $total_deduc;

                        $txt1 = $vl->addinfo_atm.''.$vl->lname.','.$vl->fname.' '.$vl->mname[0];

                        $salary = getWeekSalary($vl->username,$net,$i);

                        $tbl = new App\Payroll\TestTable;
                        $tbl->empcode = $vl->username;
                        $tbl->basic = $sl;
                        $tbl->deduc = $total_deduc;
                        $tbl->net = $net;
                        

                        $txtctr = strlen($txt1);

                        $salary = str_replace(array('.', ','), '' , $salary);

                        
                        
                        //SPACES IN BETWEEN TO REACH 73 characters
                        $chrs = 73 - ($txtctr + 23);
                        $spaces = str_repeat(' ', $chrs);

                        //ZERO BEFORE SALARY
                        $txt2 = $salary.$atm.$i;
                        $txtctr = strlen($txt2);
                        $chrs = 23 - ($txtctr);
                        $zeros = str_repeat('0', $chrs);

                        $txt .= $vl->addinfo_atm.''.$vl->lname.','.$vl->fname.' '.$vl->mname[0].$spaces.$zeros.$txt2.str_repeat(' ', 7)."\n";

                        $tbl->val = $vl->addinfo_atm.''.$vl->lname.','.$vl->fname.' '.$vl->mname[0].$spaces.$zeros.$txt2.str_repeat(' ', 7)."\n";
                        $tbl->save();

                }
                $localDisk->put('PC'.request()->payrollmon.'WK'.$i.'.txt', $txt); 
            }
                                  
            
        

        return redirect('payroll/process');
    }

    public function test()
    {

    $salarytbl = App\SalaryTable::first();

    $array = Excel::toArray(new SalaryImport, storage_path('app/salarysched/'.$salarytbl['salary_filename'].'.xlsx'));

    return $array[0];

    // $collection = Excel::toCollection(new SalaryImport, );

        foreach($array AS $index => $val)
        {
            echo $val[$index][0];
        }
    
    }

    public function mcprocess()
    {
        //MAX CHARACTER
        $max = 73;
        $max2 = 23;

        //CONST FOR LANDBANK
        $atm = "1890000";

        $folder = request()->mcpath;

        //return $folder;

        if (!file_exists($folder)) 
        {
            Storage::disk('mc')->makeDirectory($folder);

            $fsMgr = new FilesystemManager(app());
            // local disk
            $localDisk = $fsMgr->createLocalDriver(['root' => storage_path('app/mc/')]);

                //GET ALL ACTIVE EMPLOYEE
                $emp = App\View_user::get();
                $txt = "";


                //GET MC
                $mc = App\Payroll\MCView::where('payroll_mon',request()->mc_mon)->where('payroll_yr',request()->mc_year)->orderBy('lname')->orderBy('fname')->get();
                $totalmc = 0;
                
                foreach ($mc as $key => $value)
                {

                        switch($value->employment_id)
                        {
                            case 1:
                            case 11:
                            case 13:
                            case 14:
                            case 15:
                                ++$totalmc;
                                $hp = $value->salary * $value->hprate;

                                //GET SALA
                                $sala = App\Employee_sala::where('user_id',$value->userid)->where('sala_mon',request()->mc_mon)->where('sala_year',request()->mc_year)->first();

                                if($sala)
                                {
                                    //GET MON LESS 1
                                    $m = request()->mc_mon;
                                    $y = request()->mc_year;
                                    if($m == 1)
                                    {
                                        $m = 12;
                                        $y = $y - 1;
                                    }
                                    else
                                    {
                                        --$m;
                                    }

                                }
                                else
                                {
                                    $sa = 0;
                                    $la = 0;
                                }

                                if($value->employment_id == 15)
                                {
                                    $sa = 0;
                                    $la = 0;
                                    $hp = 0;
                                }
                    
                                $txt1 = $value->addinfo_atm.''.$value->lname.','. $value->fname.' '. $value->mname[0].' '.$value->exname;

                                $txtctr = strlen($txt1);

                                //SPACES IN BETWEEN TO REACH 73 characters
                                $chrs = 73 - ($txtctr + 23);
                                $spaces = str_repeat(' ', $chrs);

                                //GET MC

                                //ZERO BEFORE SALARY
                                $net_mc = number_format(($value->sa + $value->la + $value->lp + $hp) - ($value->hmo + $value->gsis + $value->pmpc + $value->gfal + $value->cdc + $value->landbank + $value->itw),2);
                                $net_mc = str_replace(array('.', ','), '' , $net_mc);
                                $txt2 = $net_mc.$atm."1";
                                $txtctr = strlen($txt2);
                                $chrs = 23 - ($txtctr);
                                $zeros = str_repeat('0', $chrs);

                                $txt .= $txt1.$spaces.$zeros.$txt2.str_repeat(' ', 7)."\n";
                            
                            }
                    
                    
                }
            $filename = 'MC'.$folder.'.txt';
            $localDisk->put($filename, $txt);

            $mc = new App\Payroll\MCProcess;
            $mc->payroll_mon = request()->mc_mon;
            $mc->payroll_year = request()->mc_year;
            $mc->txt_file = $filename;
            $mc->save();
        }
        else
        {
            return "folder already exist";
        }

        return redirect('payroll/mc/'.request()->mc_mon.'/'.request()->mc_year);
    }

}

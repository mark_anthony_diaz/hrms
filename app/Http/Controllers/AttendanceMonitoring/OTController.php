<?php

namespace App\Http\Controllers\AttendanceMonitoring;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use Auth;
use Carbon\Carbon;

class OTController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {   
     	return view('dtr.request-ot');   
    }

    public function send()
    {
        if(request()->request_cto == 'request_ot')
            {
                $this->addOT(date('Y-m-d',strtotime(request()->leave_duration3)));
                return redirect('/');
            }
            else
            {
                if($this->requestCTO(date('Y-m-d',strtotime(request()->leave_duration3))))
                {
                    return redirect('/');
                }
                else
                {
                    return view('error-message')->with('error_message','Not enough leave balance..');
                }
            }
    }

    private function addOT($dt)
    {       
            $user = App\User::where('id',request()->userid2)->first();
            //GET DURATION
            switch (request()->leave_time) {
                case 'wholeday':
                        $deduc = 1;
                    break;
                
                default:
                        $deduc = 0.5;
                    break;
            }

            //IF DIRECTOR
            $diretor = 'NO';
            if($user['usertype'] == 'Director')
                $diretor = 'YES';

            $request = new App\RequestOT;
            $request->userid = $user['id'];
            $request->empcode = $user['username'];
            $request->director = $diretor;
            $request->employee_name = $user['lname'].', '.$user['fname'].' ' .$user['mname'];
            $request->division = $user['division'];
            $request->ot_date = $dt;
            $request->ot_hours = request()->cto_hours;
            $request->ot_deduction = $deduc;
            $request->ot_deduction_time = request()->leave_time;
            $request->ot_purpose= request()->ot_purpose;
            $request->ot_output = request()->ot_output;
            $request->save();

            $tblid = $request->id;

            add_ot_leave($user['id'],$tblid,$dt,"Requested");
        
    }

    private function requestCTO($dt)
    {
        $code = randomCode(15);
         //GET DURATION
         switch (request()->leave_time) {
            case 'wholeday':
                    $deduc = 1;
                break;
            default:
                    $deduc = 0.5;
                break;
        }

        $bal = getLeaves(request()->userid2,5);
        $pending = getPending(5,request()->userid2);
        $projected = $bal - $pending;
        $rem_bal = $projected - $deduc;


        if($rem_bal < 0)
        {
            return false;
        }
        else
        {
             //GET USER INFO
            $user = App\User::where('id',request()->userid2)->first();
            
            //IF DIRECTOR
            $director = 'NO';
            if($user['usertype'] == 'Director')
                $director = 'YES';

            $request = new App\Request_leave;
            // $request->user_id = Auth::user()->id;
            $request->user_id = $user['id'];
            $request->empcode = $user['username'];
            $request->director = $director;
            $request->user_div = $user['division'];
            $request->leave_date_from = $dt;
            $request->leave_date_to = $dt;

            $request->parent = 'YES';
            $request->parent_leave = $code;
            $request->parent_leave_code = $code;
            
            $request->leave_id = 5;
            $request->leave_deduction = $deduc;
            $request->leave_deduction_time = request()->leave_time;
            $request->save();

            $tblid = $request->id;

            add_history_leave($user['id'],5,$tblid,$dt,'Requested');

            return true;
        }
       

    }

    public function pdf()
    {   
        //GET TO DETAILS
        $ot = App\RequestOT::where('id',request()->req_id)->first();

        //CTO
        $cto_yes = "&#9744";
        $cto_no = "&#9745";

        //IF CTO
        if($ot['cto'] == 'YES')
        {
            $cto_yes = "&#9745";
            $cto_no = "&#9744";
        }

        if($ot->ot_status == 'Pending')
        {
            $cto_yes = "&#9744";
            $cto_no = "&#9744";
        }

        //IF DIRECTOR
        $director = 'REYNALDO V. EBORA';
        if($ot['director'] == 'NO')
            $director = getDirector(Auth::user()->division);


        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('<!DOCTYPE html>
                            <html>
                            <head>
                              <title>HRMIS - T.O</title>
                              <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                            </head>
                            <style type="text/css">
                                @page {
                                  size: 21cm 29.7cm;
                                  margin: 20;
                                }
                                body
                                {
                                    font-family: DejaVu Sans;
                                }
                                th,td
                                {
                                    border:1px solid #555;
                                    font-size:11px;
                                }
                            </style>
                            <body>

                            <table width="100%" cellspacing="0" cellpadding="2">
                                <tr>
                                  <td style="border : 1px solid #FFF;width:20%" align="right">
                                    <img src="'.url('img/DOST.png').'" style="width:100px">
                                  </td>
                                  <td style="border : 1px solid #FFF;font-size:12px;" align="center">
                                        Republic of the Philippines<br/>
                                        PHILIPPINE COUNCIL FOR AGRICULTURE, AQUATIC AND NATURAL RESOURCES
                                        RESEARCH AND DEVELOPMENT<br/>
                                        Los Baños, Laguna
                                  </td>
                                  <td style="border : 1px solid #FFF;font-size:12px;width:20%" >

                                  </td>
                                </tr>
                            </table>
                                    <center>
                                    <h4><b>ORDER TO RENDER OVERTIME SERVICES</b></h4>
                                    </center>
                            <table border="1" celpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td colspan="3"><b>A. Subject to existing government rules and regulation, the following staff are hereby instructed to render overtime services</b></td>
                                <td colspan="5" align="center"><b>B. Time Record</b></td>
                            </tr>
                            <tr>
                                <td align="center" style="width:35% !important" rowspan="2">NAME AND DESIGNATION</td>
                                <td align="center" colspan="2" style="width:30% !important">DATE</td>
                                <td align="center" style="width:14%;!important" rowspan="2">DATE</td>
                                <td align="center" style="width:8%;!important" rowspan="2">IN</td>
                                <td align="center" style="width:8%;!important" rowspan="2">OUT</td>
                                <td align="center" style="width:10%;font-size:8px!important" rowspan="2">Signature</td>
                                <td align="center" style="width:10%;font-size:8px!important" rowspan="2">No.of Hours</td>
                            </tr>
                            <tr>
                                <td align="center"><b>From</b></td>
                                <td align="center"><b>To</b></td>
                            </tr>
                            <tr>
                                <td>'.$ot['employee_name'].'</td>
                                <td align="center">'.$ot['ot_date'].'</td>
                                <td align="center">'.$ot['ot_date'].'</td>
                                <td align="center">'.$ot['ot_date'].'</td>
                                <td align="center">'.$ot['ot_in'].'</td>
                                <td align="center">'.$ot['ot_out'].'</td>
                                <td></td>
                                <td align="center">'.countTotalTimeDiff($ot['ot_in'],$ot['ot_out'],$ot['ot_date']).'</td>
                            </tr>
                            <tr valign="bottom">
                                <td colspan="3"></td>
                                <td colspan="5" rowspan="4">
                                <b>Please check appropriate item:</b><br>
                                '.$cto_yes.' For CTO Credit<br/>
                                '.$cto_no.' Non-CTO Credit<br/>
                                </td>
                            </tr>

                            <tr valign="top">
                                <td><b>Purpose</b> : (Specifiy the activity/report/project to be accomplished indicating the deadline)<br/>'.$ot['ot_purpose'].'</td>
                                <td colspan="2" valign="top"><b>Expected Output</b> :<br/>'.$ot['ot_output'].'</td>
                            </tr>
                            <tr>
                                <td colspan="3">Name and designation of immediate supervisor who will oversee overtime work
                                <br>
                                <br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">This order of overtime is being issued as the work is really urgent and cannot be finished on time during regular office hours. Furthermore, the undersigned
                                is responsible in ensuring that the date and purpose(s) indicated above will be observed by the concerned.
                                </td>
                            </tr>

                            <tr valign="top">
                                <td colspan="3">
                                <br>
                                <br>
                                    <center>
                                    <u><b>'.strtoupper($ot['ot_supervisor']).' </u></b><br/>
                                    Supervisor
                                    <br>
                                    <br>
                                    <br>
                                    <b><u>'.$director.'</u></b><br/>
                                    Agency Head/Division Director
                                    </center>
                                </td>
                                <td colspan="5">
                                    <table style="border:1px solid #FFF;width:100%;height:135px">
                                    <tr valign="top" style="border:1px solid #FFF">
                                        <td style="border:1px solid #FFF"><b>Certified Correct:</b></td>
                                    </tr>
                                    <tr valign="bottom" style="border:1px solid #FFF">
                                        <td style="border:1px solid #FFF">
                                            <center>
                                            <b><u>'.$director.'</u></b><br/>
                                            Agency Head/Division Director
                                            </center>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            </body>
                            </html>')
        ->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function batchPDF()
    {   
        //GET ALL OT
        $mon = request()->ot_batch_mon;
        $yr = request()->ot_batch_year;

        $req = App\RequestOT::where('division',Auth::user()->division)->whereIn('ot_status',['Approved','Pending'])->get();
        // $req = App\RequestOT::get();

        //ROW
        $rows = "";
        foreach ($req as $key => $value) {
            $rows .= "
                    <tr>
                        <td>".$value->employee_name."</td>
                        <td>".$value->ot_purpose."</td>
                        <td>".$value->ot_output."</td>
                        <td align='center'>".$value->ot_date."</td>
                        <td align='center'>".$value->ot_in."</td>
                        <td align='center'>".$value->ot_out."</td>
                        <td align='center'>".countTotalTimeDiff($value->ot_in,$value->ot_out,$value->ot_date)."</td>
                        <td align='center'>".$value->cto."</td>
                        <td></td>
                    </tr>
                    ";
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('<!DOCTYPE html>
                            <html>
                            <head>
                              <title>HRMIS - O.T</title>
                              <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                            </head>
                            <style type="text/css">
                                body
                                {
                                    font-family: DejaVu Sans;
                                }
                                th,td
                                {
                                    border:1px solid #555;
                                    font-size:11px;
                                }
                            </style>
                            <body>

                            <table width="100%" cellspacing="0" cellpadding="2">
                                <tr>
                                  <td style="border : 1px solid #FFF;width:20%" align="right">
                                    <img src="'.url('img/DOST.png').'" style="width:100px">
                                  </td>
                                  <td style="border : 1px solid #FFF;font-size:12px;" align="center">
                                        Republic of the Philippines<br/>
                                        PHILIPPINE COUNCIL FOR AGRICULTURE, AQUATIC AND NATURAL RESOURCES
                                        RESEARCH AND DEVELOPMENT<br/>
                                        Los Baños, Laguna
                                  </td>
                                  <td style="border : 1px solid #FFF;font-size:12px;width:20%" >

                                  </td>
                                </tr>
                            </table>
                                    <center>
                                    <h4><b>ORDER TO RENDER OVERTIME SERVICES</b></h4>
                                    '.date('F',mktime(0, 0, 0, $mon, 10)).' '.$yr.'
                                    </center>
                            
                                <table width="100%" cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td><b>Name of Staff</b></td>
                                        <td><b>Purpose</b></td>
                                        <td><b>Expected Output</b></td>
                                        <td align="center"><b>Date</b></td>
                                        <td style="width:50px !important" align="center"><b>IN</b></td>
                                        <td style="width:100px !important" align="center"><b>OUT</b></td>
                                        <td style="width:100px !important" align="center"><b>No. of Hours</b></td>
                                        <td align="center"><b>CTO</b></td>
                                        <td align="center"><b>Signature</b></td>
                                    </tr>
                                    '.$rows.'
                                </table>
                                <br>
                                <br>
                                <p>'.getDirector(Auth::user()->division).'</p>
                            </body>
                            </html>')
        ->setPaper('legal', 'landscape');
        return $pdf->stream();
    }
}

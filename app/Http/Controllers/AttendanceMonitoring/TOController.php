<?php

namespace App\Http\Controllers\AttendanceMonitoring;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use Auth;
use Carbon\Carbon;

class TOController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {   
     	return view('dtr.request-to');   
    }

    public function send()
    {
        //GET DETAIL
        
        if(request()->to_id)
        {
            $req = App\RequestTO::where('id',request()->to_id)->first();
            $user = App\User::where('id',$req['userid'])->first();
        }
        else
        {
            $user = App\User::where('id',request()->userid2)->first();
        }


        $duration = explode('-',request()->leave_duration4);

        $from = Carbon::parse($duration[0]);
        $from_orig = Carbon::parse($duration[0]);
        $to = Carbon::parse($duration[1]);

        // $dt = Carbon::parse(request()->leave_duration3);

        // $this->addTO(date('Y-m-d',strtotime($dt)));

        $diff = 1+($from->diffInDays($to));

        // CHECK IF SINGLE DATE
        if($diff == 1)
        {
            $this->addTO(1,$from,$to,$user,request()->to_id);
        }
        else
        {
            $this->addTO(2,$from,$to,$user,request()->to_id);
        }

        //ADD HISTORY ON LEAVE

        if(request()->active_tab)
        {
            switch (request()->active_tab) {
                case 'TO':
                        return redirect('request-for-approval');
                    break;
                
                default:
                        return redirect('/');
                    break;
            }
        }
        else
        {
            return redirect('/');
        }
        
    }

    private function addTO($type,$from,$to,Object $user,$toid = null)
    {
        //IF EDIT T.O, DELETE NA LANG LUMA THEN INSERT NG BAGO
        //BUT REMAIN NAG STATUS

        //CHECK IF EDIT
        if($toid)
        {
            $req = App\RequestTO::where('id',$toid)->first();
            $to_status = $req['to_status'];
            $to_status_by = getStaffInfo(Auth::user()->id,'fullname');
            $to_status_date = $req['to_status_date'];

            //THEN DELETE ANG LUMA
            App\RequestTO::where('parent_to_code',$req['parent_to_code'])->delete();
        }
        else
        {
            $to_status = "Pending";
            $to_status_by = null;
            $to_status_date = null;
        }

            //GET DURATION
            if($type == 1)
            {
                switch (request()->leave_time) {
                case 'wholeday':
                        $deduc = 1;
                    break;
                
                default:
                        $deduc = 0.5;
                    break;
                }
            

                $code = randomCode(15);
                

                $request = new App\RequestTO;
                $request->userid = $user['id'];
                $request->empcode = $user['username'];
                $request->employee_name = $user['lname'].', '.$user['fname'].' ' .$user['mname'];
                $request->division = $user['division'];
                $request->to_date_from = $from;
                $request->to_date_to = $to;
                $request->to_total_day = $deduc;
                $request->parent = 'YES';
                $request->parent_to = $code;
                $request->parent_to_code = $code;
                $request->to_deduction = $deduc;
                $request->to_deduction_time = request()->leave_time;
                $request->to_vehicle = request()->vehicle;
                $request->to_perdiem = request()->perdiem;
                $request->to_place = request()->place;
                $request->to_purpose = request()->purpose;
                $request->to_status = $to_status;
                $request->to_status_by = $to_status_by;
                $request->to_status_date = $to_status_date;
                $request->save();

                $tblid = $request->id;

            }
            else
            {
                $code = randomCode(15);
                
                $orig_from = $from;

                $diff = 1+($from->diffInDays($to));

                $total_deduc = 0;

                for($i = 1; $i <= $diff; $i++)
                    {
                        if($i == 1)
                            {
                                $dt = date('Y-m-d',strtotime($from));
                                $orig_from = $dt;
                            }
                            else
                            {
                                $dt = $from->addDays(1);             
                            }

                            // if(!$this->checkIfWeekend($dt))
                            // {
                            //     if(!checkIfHoliday($dt))
                            //     {  
                            //         $total_deduc++;
                            //     }
                            // }
                            $total_deduc++;

                            $request = new App\RequestTO;
                            $request->userid = $user['id'];
                            $request->empcode = $user['username'];
                            $request->employee_name = $user['lname'].', '.$user['fname'].' ' .$user['mname'];
                            $request->division = $user['division'];
                            $request->to_date_from = $dt;
                            $request->to_total_day = 1;
                            $request->parent_to_code = $code;
                            $request->to_status = $to_status;
                            $request->to_status_by = $to_status_by;
                            $request->to_status_date = $to_status_date;
                            $request->save();    

                    }

                $request = new App\RequestTO;
                $request->userid = $user['id'];
                $request->empcode = $user['username'];
                $request->employee_name = $user['lname'].', '.$user['fname'].' ' .$user['mname'];
                $request->division = $user['division'];
                $request->to_date_from = $orig_from;
                $request->to_date_to = $to;
                $request->to_total_day = $total_deduc;
                $request->parent = 'YES';
                $request->parent_to = $code;
                $request->parent_to_code = $code;
                $request->to_deduction = $total_deduc;
                $request->to_deduction_time = request()->leave_time;
                $request->to_vehicle = request()->vehicle;
                $request->to_perdiem = request()->perdiem;
                $request->to_place = request()->place;
                $request->to_purpose = request()->purpose;
                $request->to_status = $to_status;
                $request->to_status_by = $to_status_by;
                $request->to_status_date = $to_status_date;
                $request->save();
            }

            // add_to_leave(Auth::user()->id,$tblid,$dt,"Requested");
        
    }

    public function checkIfWeekend($dt)
    {
        $dt = Carbon::parse($dt);

        if($dt->isWeekend())
            return true;
        else
            return false;
    }

    public function pdf()
    {
        //GET TO DETAILS
        $to = App\RequestTO::where('id',request()->req_id)->first();

        //GET EMPLOYEE DETAILS
        // $emp = App\User::where('id',$to[''])

        $plantilla = getPlantillaInfo($to['empcode']);

        //VEHICLE
        $to_vehicle_1 = "&#9744";
        $to_vehicle_2 = "&#9744";
        $to_vehicle_3 = "&#9744";

        if($to['to_vehicle'] == 'Official')
            $to_vehicle_1 = "&#9745";
        elseif($to['to_vehicle'] == 'Personal')
            $to_vehicle_2 = "&#9745";
        else
            $to_vehicle_3 = "&#9745";


        if($to['to_date_from'] == $to['to_date_to'])
        {
            $to_date = date('F d, Y',strtotime($to['to_date_from']));
        }
        else
        {
            $to_date = date('F d, Y',strtotime($to['to_date_from']))." - ".date('F d, Y',strtotime($to['to_date_to']));
        }

        //WHOLEDAY AM OR PM
        
        if($to['to_deduction_time'] == 'wholeday')
        {
            $time = "(WD)";
        }
        elseif($to['to_deduction_time'] == 'AM' || $to['to_deduction_time'] == 'PM')
        {
            $time = "(".$to['to_deduction_time'].")";
        }
        else
        {
            $time = "";
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('<!DOCTYPE html>
                            <html>
                            <head>
                              <title>HRMIS - T.O</title>
                              <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                            </head>
                            <style type="text/css">
                                @page {
                                  size: 21cm 29.7cm;
                                  margin: 20;
                                }
                                body
                                {
                                    font-family: DejaVu Sans;
                                }
                                th,td
                                {
                                    border:1px solid #555;
                                    font-size:11px;
                                }
                            </style>
                            <body>

                            <table width="100%" cellspacing="0" cellpadding="2">
                                <tr>
                                  <td style="border : 1px solid #FFF;width:20%" align="right">
                                    <img src="'.url('img/DOST.png').'" style="width:100px">
                                  </td>
                                  <td style="border : 1px solid #FFF;font-size:12px;" align="center">
                                        Republic of the Philippines<br/>
                                        PHILIPPINE COUNCIL FOR AGRICULTURE, AQUATIC AND NATURAL RESOURCES
                                        RESEARCH AND DEVELOPMENT<br/>
                                        Los Baños, Laguna
                                  </td>
                                  <td style="border : 1px solid #FFF;font-size:12px;width:20%" >

                                  </td>
                                </tr>
                            </table>
                                    <center>
                                    <h4><b>TRAVEL ORDER</b></h4>
                                    <h5><i>Permission to travel is hereby granted</i></h5>
                                    </center>

                            <table width="100%" cellspacing="0" cellpadding="2" border="0" style="font-size:13px!important">
                                <tr>
                                    <td align="right" style="width:45%;border:1px #FFF solid">
                                    NAME:
                                    </td>
                                    <td align="left" style="border:1px #FFF solid">
                                    '.$to['employee_name'].'
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="border:1px #FFF solid">
                                    DESIGNATION:
                                    </td>
                                    <td align="left" style="border:1px #FFF solid">
                                    '.$plantilla['position_desc'].'
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="border:1px #FFF solid">
                                    DESTINATION:
                                    </td>
                                    <td align="left" style="border:1px #FFF solid">
                                    '.$to['to_place'].'
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="border:1px #FFF solid">
                                    DATE OF TRAVEL:
                                    </td>
                                    <td align="left" style="border:1px #FFF solid">
                                    '.$to_date.' '.$time.'
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="border:1px #FFF solid">
                                    PURPOSE:
                                    </td>
                                    <td align="left" style="border:1px #FFF solid">
                                    '.$to['to_purpose'].'
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border:1px #FFF solid" colspan="2">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="border:1px #FFF solid" valign="top">
                                    SOURCE OF FUND:
                                    </td>
                                    <td align="left" style="border:1px #FFF solid" valign="top">
                                    &#9745 Fund 101
                                    <br/>
                                    &#9744 Fund 102
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="border:1px #FFF solid" valign="top">
                                    TRANSPORTATION:
                                    </td>
                                    <td align="left" style="border:1px #FFF solid" valign="top">
                                    '.$to_vehicle_1.' Official Vehicle
                                    <br/>
                                    '.$to_vehicle_3.' Non-PCAARRD
                                    <br/>
                                    '.$to_vehicle_2.'  Personal
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border:1px #FFF solid" colspan="2">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border:1px #FFF solid" colspan="2" align="center"><u>
                                    '.getDirectorNoDesc(Auth::user()->division,$to['userid']).'</u><br><small><b>Approving Authority</b></small>
                                    </td>
                                </tr>
                            </table>
                            </body>
                            </html>')
        ->setPaper('a4', 'portrait');
        return $pdf->stream();
    }
}

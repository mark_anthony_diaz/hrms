<?php

namespace App\Http\Controllers\AttendanceMonitoring;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App;
use Carbon\Carbon;
use Auth;

class FinalProcess extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        //GET ALL STAFF
        $staff = collect(App\View_user::whereIn('id',request()->check_request)->where('usertype','!=','Administrator')->orderBy('lname')->orderBy('fname')->get());

        $data = collect([]);


        
        foreach ($staff->all() as $staffs)
        {

            
            if(!checkIfProcess($staffs->id,request()->mon,request()->yr))
            {

            $process_code = randomCode(45);
                            
            $deficit = 0.000;

            $totaldeduction = 0.000;
            $totalvldeduction = 0.000;

            $vl_ctr = 0;

            $remarks = "";

            $remarks .= "<b>".$staffs->lname.", ".$staffs->fname."</b><br/>---Current Leave Balances---<br/>";


            $collecleave = collect([]);

            //LEAVE
            $orig_vl = 0.000;
            $orig_sl = 0.000;
            $orig_pl = 0.000;
            $orig_spl = 0.000;
            $orig_cto = 0.000;
            $orig_fl = 0.000;
            $orig_MatL = 0.000;
            $orig_PatL = 0.000;
            $orig_StL = 0.000;
            $orig_ReL = 0.000;
            $orig_el = 0.000;
            $orig_spec = 0.000;
            $orig_ml = 0.000;
            $orig_ua = 0.000;
            $orig_slwop = 0.000;

            //GET STAFF CURRENT LEAVE BALANCES
            foreach(showLeaves() AS $leaves){

                // switch ($leaves->id) {
                //     case 1:
                //                 $lv = getLVSummary(1,$staffs->id,request()->mon,request()->yr);
                //         break;
                //     case 2:
                //                 $lv = getLVSummary(2,$staffs->id,request()->mon,request()->yr);
                //         break;
                    
                //     default:
                //                 $lv = getLeaves($staffs->id,$leaves->id);
                //         break;
                // }
                
                $lv = getLeaves($staffs->id,$leaves->id);  
                $remarks .= $leaves->leave_desc." : <b>$lv</b><br>";

                //GET PREVIOUS LEAVE
                switch ($leaves->id)
                {
                        case 1:
                            # code...
                                $orig_vl += $lv;
                            break;
                        case 2:
                            # code...
                                $orig_sl += $lv;
                            break;
                        case 3:
                            # code...
                                $orig_pl += $lv;
                            break;
                        case 4:
                            # code...
                                $orig_spl += $lv;
                            break;
                        case 5:
                            # code...
                                $orig_cto += $lv;
                            break;
                        case 6:
                            # code...
                                $orig_fl += $lv;
                            break;
                        case 7:
                            # code...
                                $orig_MatL += $lv;
                            break;
                        case 8:
                            # code...
                                $orig_PatL += $lv;
                            break;
                        case 9:
                            # code...
                                $orig_StL += $lv;
                            break;
                        case 10:
                            # code...
                                $orig_ReL += $lv;
                            break;
                        case 11:
                            # code...
                                $orig_el += $lv;
                            break;
                        case 12:
                            # code...
                                $orig_spec += $lv;
                            break;
                        case 13:
                            # code...
                                $orig_el += $lv;
                            break;
                        case 14:
                            # code...
                                $orig_ua += $lv;
                            break;
                        case 15:
                            # code...
                                $orig_slwop += $lv;
                            break;
                    }

                $collecleave->put($leaves->leave_desc, $lv);
            }
            
            $remarks .= "<br/><br/>";

            
            if($staffs->dtr_exe != 1 || $staffs->employement_id == 15)
            {
                if($staffs->employement_id != 12)
                {
                    //GET L.W.P
                    $lwp = getLWP($staffs->id,request()->mon,request()->yr);

                    $lwp = explode("|", $lwp);

                    $no_lates = 

                    //COUNT LWP
                    $nolwp = countLWP($staffs->id,request()->mon,request()->yr);
                    // $earnleave = getLWPCount((string)$nolwp);
                    $earnleave = 1.25;

                    // return $lwp[0];


                    $remarks .= "Required Total Hours: <b>".$lwp[10]."</b><br>";
                    $remarks .= "Total Hours Rendered: <b>".$lwp[11]."</b><br>";
                    $deficit_hours = $lwp[10] - $lwp[11];
                    $remarks .= "Deficit hours: <b>".$deficit_hours."</b><br><br>";

                    //ABSENT
                    $remarks .= "---Absent---<br/>Total : <b>".$lwp[0]."</b><br/><br/>";

                    $remarks .= "---Leave Without Pay---<br/>";

                    // if($lwp[0] >= 0)
                    // {
                    //     //EARNED VL/SL
                    //     $lwps = getLWPCount($lwp[0]);

                    //     $remarks .= "LWP total : <b>".$nolwp."</b><br>";
                    // }
                    $remarks .= "LWP total : <b>".$nolwp."</b><br>";

                    $remarks .= "<br/>";

                    $remarks .= "VL/SL earn : <b>".$earnleave."</b><br><br>";

                    $remarks .= "---Deduction---<br/>";
                    
                    //GET LATES/UNDERTIME
                    $remarks .= $lwp[2]."<br/>Lates/Undertime Deduction : <b>".number_format((float)$lwp[1], 3, '.', '')."</b><br><br>";

                    $totaldeduction += number_format((float)$lwp[1], 3, '.', '');

                    $no_process_lates = $lwp[3]."h ".$lwp[4]."m";
                    $no_process_under = $lwp[8]."h ".$lwp[9]."m";
                    $no_process_lates_under = $lwp[5]."h ".$lwp[6]."m";
                    $no_lates_total = $lwp[7];
                    $no_process_absent = $lwp[0];
                }
                else
                {
                    $totaldeduction = 0.000;
                    $earnleave = 1.25;
                    $remarks .= "VL/SL earn : <b>".$earnleave."</b><br><br>";

                    $nolwp = 0;

                    $no_process_lates = "0h 0m";
                    $no_process_under = "0h 0m";
                    $no_process_lates_under = "0h 0m";
                    $no_lates_total = "0h 0m";
                    $no_process_absent = 0;
                }
            }
            else
            {
                $totaldeduction = 0.000;
                $earnleave = 1.25;
                $remarks .= "VL/SL earn : <b>".$earnleave."</b><br><br>";

                $nolwp = 0;

                $no_process_lates = "0h 0m";
                $no_process_under = "0h 0m";
                $no_process_lates_under = "0h 0m";
                $no_lates_total = "0h 0m";
                $no_process_absent = 0;

            }
            

            //GET REQUEST
            $remarks .= "---Request---<br/>";

            //MULTIPLE DATES
            $leave_req_0 = collect(App\Request_leave::whereNull('parent_leave')->where('user_id',$staffs->id)->where('leave_action_status','Approved')->whereMonth('leave_date_from',request()->mon)->whereYear('leave_date_from',request()->yr)->get());

            //SINGLE DATE
            $leave_req_1 = collect(App\Request_leave::where('parent','YES')->where('leave_deduction',1)->where('user_id',$staffs->id)->where('leave_action_status','Approved')->whereMonth('leave_date_from',request()->mon)->whereYear('leave_date_from',request()->yr)->get());

            $leave_req = $leave_req_0->merge($leave_req_1);

            if($leave_req)
            {
                $leave_req = $leave_req->all();
                $vl = 0.000;
                $sl = 0.000;
                $pl = 0.000;
                $spl = 0.000;
                $cto = 0.000;
                $fl = 0.000;
                $MatL = 0.000;
                $PatL = 0.000;
                $StL = 0.000;
                $ReL = 0.000;
                $el = 0.000;
                $spec = 0.000;
                $ml = 0.000;
                $ua = 0.000;
                $slwop = 0.000;

                //UPDATE TO PROCESSED
                foreach ($leave_req as $leaves) 
                {
                    $remarks .= getLeaveInfo($leaves->leave_id)." - Date : ".$leaves->leave_date_from." to ".$leaves->leave_date_to." - Deduction : ".$leaves->leave_deduction."<br>";

                    switch ($leaves->leave_id) 
                    {
                        case 1:
                        # code...
                                $vl += $leaves->leave_deduction;
                                $totalvldeduction = $totaldeduction + $leaves->leave_deduction;

                                $fl += $leaves->leave_deduction;

                                $vl_ctr += $leaves->leave_deduction;

                                $this->addToMCDays($staffs->id,$staffs->username,'Vacation Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);

                            break;
                        case 2:
                            # code...
                                $sl += $leaves->leave_deduction;
                                
                                $this->addToMCDays($staffs->id,$staffs->username,'Sick Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 3:
                            # code...
                                $pl += $leaves->leave_deduction;

                                $this->addToMCDays($staffs->id,$staffs->username,'Privilege Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 4:
                            # code...
                                $spl += $leaves->leave_deduction;

                                $this->addToMCDays($staffs->id,$staffs->username,'Solo Parent Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 5:
                            # code...
                                $cto += $leaves->leave_deduction;
                            break;
                        case 6:
                            # code...
                                $fl += $leaves->leave_deduction;
                                $vl += $leaves->leave_deduction;

                                $vl_ctr += $leaves->leave_deduction;

                                $totalvldeduction = $totaldeduction + $leaves->leave_deduction;

                                $this->addToMCDays($staffs->id,$staffs->username,'Force Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 7:
                            # code...
                                $MatL += $leaves->leave_deduction;
                                $this->addToMCDays($staffs->id,$staffs->username,'Maternity Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 8:
                            # code...
                                $PatL += $leaves->leave_deduction;
                                $this->addToMCDays($staffs->id,$staffs->username,'Paternity Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 9:
                            # code...
                                $StL += $leaves->leave_deduction;
                            break;
                        case 10:
                            # code...
                                $ReL += $leaves->leave_deduction;
                                $this->addToMCDays($staffs->id,$staffs->username,'Rehabilitation Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 11:
                            # code...
                                $el += $leaves->leave_deduction;
                                $this->addToMCDays($staffs->id,$staffs->username,'Emergency Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        case 12:
                            # code...
                                $spec += $leaves->leave_deduction;
                                $this->addToMCDays($staffs->id,$staffs->username,'Special Leave (Magna Carta of Women) Leave',$leaves->leave_deduction,$leaves->leave_date_from,$leaves->leave_date_to,$process_code);
                            break;
                        // case 13:
                        //     # code...
                        //         $el += $leaves->leave_deduction;

                        //     break;
                        case 14:
                            # code...
                                $ua += $leaves->leave_deduction;
                            break;
                        case 15:
                            # code...
                                $slwop += $leaves->leave_deduction;
                            break;
                    }

                    App\Request_leave::where('id',$leaves->id)
                                    ->update([
                                                // 'leave_action_status' => 'Processed',
                                                'process_code' => $process_code
                                            ]);            
                }

                $remarks .= " LEAVE : ".$totaldeduction."<br>";
            }
            
            $remarks .= "<br/>Total VL Deduction : ".($vl_ctr + $totaldeduction)."<br/>";

            $remarks .= "<br/>---Ending Leave Balances---<br/>";

            foreach ($collecleave as $keybal => $valuebal) 
            {
                
                switch ($keybal) {
                    case 'Vacation Leave':
                        # code...
                            $val = ($valuebal + $earnleave) - $vl;

                            $deficit = 0;
                            if($totalvldeduction > ($valuebal + $earnleave))
                            {
                                $deficit = ($vl + $totalvldeduction) - ($valuebal + $earnleave);
                            }


                            $vl_bal = $val;

                            //UPDATE LEAVE
                            if($val > -1)
                            {
                                $data->push(['leave_id' => 1,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_vl,'leave_bal' => $val,'leave_bal_nega' => $deficit,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }

                        break;
                    case 'Sick Leave':
                        # code...
                            // $val = $valuebal - $sl;
                            $val = ($valuebal + $earnleave) - $sl;

                            $sl_bal = $val;

                            $deficit = 0;
                            if($val < 0)
                            {
                                $deficit = ($sl + $totaldeduction) - ($valuebal + $earnleave);
                            }

                            //UPDATE LEAVE
                            if($val > -1)
                            {
                                $data->push(['leave_id' => 2,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_sl,'leave_bal' => $val,'leave_bal_nega' => $deficit,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }

                        break;
                    case 'Privilege Leave':
                        # code...
                            
                            $val = $valuebal - $pl;

                            if($orig_pl > 0)
                            {
                                $data->push(['leave_id' => 3,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_pl,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }


                            if(request()->mon == 12)
                            {
                                $data->push(['leave_id' => 3,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $val,'leave_bal' => 3,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                            
                        break;
                    case 'Solo Parent Leave':
                        # code...
                            $val = $valuebal - $spl;

                            if($orig_spl > 0)
                            {
                                $data->push(['leave_id' => 4,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_spl,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }

                        break;
                    case 'Compensatory Time-Off':
                        # code...
                            $val = $valuebal - $cto;

                            if($orig_cto > 1)
                            {
                                $data->push(['leave_id' => 5,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_cto,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Force Leave':
                        # code...
                            $val = $valuebal - $fl;

                            $deficit = 0;
                            //UPDATE LEAVE
                            if($val > 0)
                            {
                                $data->push(['leave_id' => 6,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_fl,'leave_bal' => $val,'leave_bal_nega' => $deficit,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                            elseif($val <= 0)
                            {
                                $data->push(['leave_id' => 6,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_fl,'leave_bal' => 0,'leave_bal_nega' => $deficit,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }


                            if(request()->mon == 12)
                            {
                                $data->push(['leave_id' => 6,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $val,'leave_bal' => 5,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }

                        break;
                    case 'Maternity Leave':
                        # code...
                            $val = $valuebal - $MatL;

                            if($orig_MatL > 0)
                            {
                                $data->push(['leave_id' => 7,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_MatL,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Paternity Leave':
                        # code...
                            $val = $valuebal - $PatL;

                            if($orig_PatL > 0)
                            {
                                $data->push(['leave_id' => 8,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_PatL,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Study Leave':
                        # code...
                            $val = $valuebal - $StL;

                            if($orig_StL > 0)
                            {
                                $data->push(['leave_id' => 9,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_StL,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Rehabilitation Leave':
                        # code...
                            $val = $valuebal - $ReL;

                            if($orig_ReL > 0)
                            {
                                $data->push(['leave_id' => 10,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_ReL,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Emergency Leave':
                        # code...
                            $val = $valuebal - $el;

                            if($orig_el > 0)
                            {
                                $data->push(['leave_id' => 11,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_el,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Monetize Leave':
                        # code...
                            $val = $valuebal - $ml;

                            if($orig_ml > 0)
                            {
                                $data->push(['leave_id' => 13,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_ml,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Special Leave (Magna Carta of Women)':
                        # code...
                            $val = $valuebal - $spec;

                            if($orig_spec > 0)
                            {
                                $data->push(['leave_id' => 12,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_spec,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }

                        break;
                    case 'Unauthorized Absence':
                        # code...
                            $val = $valuebal - $ua;

                            if($orig_ua > 0)
                            {
                                $data->push(['leave_id' => 14,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_ua,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                    case 'Sick Leave Without Pay':
                        # code...
                            $val = $valuebal - $slwop;

                            if($orig_slwop > 0)
                            {
                                $data->push(['leave_id' => 15,'user_id' => $staffs->id,'empcode' => $staffs->username,'leave_bal_prev' => $orig_slwop,'leave_bal' => $val,'leave_bal_nega' => 0,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'process_code' => $process_code]);
                            }
                        break;
                }

                $remarks .= $keybal." : <b>".$val."</b><br/>";
            }


            //PROCESS T.O

            //GET T.O
            $to_req = App\RequestTO::where('parent','YES')->where('userid',$staffs->id)->where('to_status','Approved')->whereMonth('to_date_from',request()->mon)->whereYear('to_date_from',request()->yr)->get();
            
            if($to_req)
            {
                foreach ($to_req as $key => $valueto) {
                    
                    $tos = App\RequestTO::where('id',$valueto->id)
                            ->update([
                                        // 'to_status' => "Processed",
                                        'process_code' => $process_code
                                    ]);
                    
                    if($valueto->to_total_day <= 1.0)
                    {
                        $to_mc_deduc = $valueto->to_total_day;
                    }
                    else
                    {
                        $to_mc_deduc = 1;
                    }


                    //GET DATE
                    $from = Carbon::parse($valueto->to_date_from);
                    $to = Carbon::parse($valueto->to_date_to);
                    $diff = 1+($from->diffInDays($to));

                    for($i = 1; $i <= $diff; $i++)
                    {
                        
                        if($i == 1)
                        {
                            $dt = date('Y-m-d',strtotime($from));
                            $orig_from = $dt;
                        }
                        else
                        {
                            $dt = $from->addDays(1);             
                        }


                        if(!$this->checkIfWeekend($dt))
                                {
                                    if(!checkIfHoliday($dt))
                                    {  
                                        if($valueto->to_perdiem == 'YES')
                                            {
                                                $this->addToMCDays($staffs->id,$staffs->username,'Travel',$to_mc_deduc,$dt,$dt,$process_code);
                                            }
                                    }
                                }
                    }


                    

                    //UPDATE CHILED T.O
                    App\RequestTO::whereNull('parent')->where('parent_to_code',$valueto->process_to)
                            ->update([
                                        // 'to_status' => "Processed",
                                        'process_code' => $process_code
                                    ]);
                }   
            }

            // $remarks .= "<br/>---Deficit---<br/>$deficit";

            // SAVE TO DTR SUMMARY
            $processed = new App\DTRProcessed;
            $processed->process_code = $process_code;
            $processed->userid = $staffs->id;
            $processed->empcode = $staffs->username;
            $processed->employee_name = $staffs->lname . ", " .$staffs->fname . " " . $staffs->mname;
            $processed->dtr_mon = request()->mon;
            $processed->dtr_year = request()->yr;
            $processed->dtr_division = $staffs->division;
            $processed->vl_leave =  $vl_ctr;
            $processed->vl_lwop = $nolwp;
            $processed->vl_late = $no_process_lates;
            $processed->vl_totalunderlate = $no_process_under;
            $processed->vl_totalunderlatededuc = $totaldeduction;
            $processed->vl_bal = $vl_bal;
            $processed->vl_undertime = $no_process_lates_under;
            $processed->sl_leave = $sl;
            $processed->nolates = $no_lates_total; 
            $processed->noabsent = $no_process_absent;
            $processed->sl_bal = $sl_bal;
            $processed->save();
            $processed_id = $processed->id;


            $lv = App\Employee_leave::insert($data->all());


            //DISAPPROVED FL
            $leave_req_fl = App\Request_leave::whereNotNull('parent')->where('user_id',$staffs->id)->where('leave_id',6)->where('leave_action_status','Disapproved')->whereMonth('leave_date_from',request()->mon)->whereYear('leave_date_from',request()->yr)->get();

                if($leave_req_fl)
                {
                    //GET TOTAL FL DISAPPROVED
                    $total_fl = 0;
                    foreach ($leave_req_fl as $key => $fls) {
                        $total_fl += $fls->leave_deduction;
                    }

                    $bal = getLeaves($staffs->id,6);      
                }

            //SAVE HP/SALA
            if($staffs->employement_id != 15)
                {
                    $this->saveHP($process_code,$staffs->id,$staffs->username,request()->mon,request()->yr);
                    $this->saveSALA($process_code,$staffs->id,$staffs->username,request()->mon,request()->yr);

                    $this->saveMC($process_code,$staffs->id,$staffs->username,request()->mon,request()->yr);
                }

            }
        }
        
        return redirect('dtr/emp/'.request()->mon.'/'.request()->yr);
        // echo $remarks."<hr/>";
    }

    public function checkIfWeekend($dt)
    {
        $dt = Carbon::parse($dt);

        if($dt->isWeekend())
            return true;
        else
            return false;
    }

    function saveHP($process_code,$userid,$username,$mon,$yr)
    {

        $m = $mon;
        $y = $yr;
        if($m == 12)
            {
                $m = 1;
                ++$y;
            }
            else
            {
                ++$m;
            }

        //GET SALARY
        $plantilla = getPlantillaInfo($username);

        $hp = new App\Employee_hp;
        $hp->user_id = $userid;
        $hp->empcode = $username;
        $hp->hp_salary = $plantilla['plantilla_salary'];
        $hp->hp_per = 15;
        $hp->hp_mon = $m;
        $hp->hp_year = $y;
        $hp->process_code = $process_code;
        $hp->save();

    }

    function saveSALA($process_code,$userid,$username,$mon,$yr)
    {
        $m = $mon;
        $y = $yr;
        if($m == 12)
            {
                $m = 1;
                $y = ++$y;
            }
            else
            {
                ++$m;
            }

        $mon2 = date('F',mktime(0, 0, 0, $m, 10));

        $totaldays = Carbon::parse($mon2.'-'.$y)->daysInMonth;
        $weekdays = 0;
        for($i = 1;$i <= $totaldays;$i++)
            {
                $dtr_date = date("Y-m-d",strtotime($y.'-'.$m.'-'.$i));
                $dayDesc = weekDesc($dtr_date);
                switch ($dayDesc) {
                    case 'Sat':
                    case 'Sun':
                        # code...
                        break;
                                    
                    default:
                             ++$weekdays;
                         break;
                }
            }
        $sa_perday = 150;
        $sa_amt = $weekdays * $sa_perday;

        $la_amt = 500;
        $la_perday = 22.73;

        //COUNT HLODAY
        $holiday = countHolidays($m,$y);

        //COUNT WORK SUSPENSION
        $worksus = countWorkSus($m,$y);

        // $noday = countDaysInOffice($userid,$mon,$yr);

        $totalnoday = $holiday + $worksus;

        $sa_total = $sa_amt - ($totalnoday * $sa_perday);
        $la_total = $la_amt - ($totalnoday * $la_perday);

        $sala = new App\Employee_sala;
        $sala->user_id = $userid;
        $sala->empcode = $username;
        $sala->fullname = getStaffInfo($userid);
        $sala->division = Auth::user()->division;
        $sala->sa_amt = $sa_total;
        $sala->la_amt = $la_total;
        // $sala->noday = $noday;
        $sala->sala_mon = $m;
        $sala->sala_year = $y;
        $sala->process_code = $process_code;
        $sala->save();
    }

    function saveMC($process_code,$userid,$username,$mon,$yr)
    {
        //GET MC DEDUC
        $dudec = App\Payroll\MCDeduc::where('fldEmpCode',$username)->whereNotNull('MC_AMOUNT')->get();
        $gsis = 0;
        $hmo = 0;
        $pmpc = 0;
        $cdc = 0;
        $gfal = 0;
        $lb = 0;

        $m = $mon;
        $y = $yr;
        if($m == 12)
            {
                $m = 1;
                $y = ++$y;
            }
            else
            {
                ++$m;
            }
        
        foreach ($dudec as $key => $value) {

            $amt = $value->MC_AMOUNT;

            //GSIS
            if($value->ORG_CODE == 1)
            {
                switch($value->SERV_CODE)
                {
                    case '319A':
                        $gfal += $amt;
                    break;

                    default:
                        $gsis += $amt;
                }
            }

            //PMPC
            if($value->ORG_CODE == 6)
            {
                if($value->SERV_CODE == '933')
                    $hmo += $amt;
                else
                    $pmpc += $amt;
            }

            //CDC
            if($value->ORG_CODE == 5)
            {
                $cdc += $amt;
            }

            //LANDBANK
            if($value->ORG_CODE == 13)
            {
                $lb += $amt;
            }
            

        }
        //GET CURRENT SALARY
        $plantilla = getPlantillaInfo($username);


        //GET ITW
        $itw = getITW($userid);

        $lp = getLP($userid);

        //ENTITLED SA 30% HP
        //43,344,83,118,80,139,213,226,99
        switch ($userid) {
            case 43:
            case 344:
            case 83: 
            case 118:
            case 80:
            case 139:
            case 213:
            case 226: 
            case 99:  
                    $hprate = 0.30;
                break;
            
            default:
                    $hprate = 0.15;
                break;
        }

        //SALA
        //GET PREVIOUS MONTH
        $m2 = $mon - 1;
        $y2 = $y;
        if($m2 == 1)
        {
            $m2 = 12;
            $y2 = --$y2;
        }
            

        //LEAVE
        //MULTIPLE DATE
        $l1_total = App\Request_leave::where('user_id',$userid)->whereNull('parent_leave')->whereNotNull('parent_leave_code')->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',$m2)->whereYear('leave_date_from',$y2)->count();

        //SINGLE DATE
        $l2_total = App\Request_leave::where('user_id',$userid)->where('parent','YES')->whereIn('leave_deduction',[1,0.5])->whereNotIn('leave_id',[5,16])->where('leave_action_status','Approved')->whereMonth('leave_date_from',$m2)->whereYear('leave_date_from',$y2)->sum('leave_deduction');                     

        //T.O
        $total_to = 0;

        //MULTIPLE
        $tos = App\RequestTO::where('userid',$userid)->where('to_perdiem','YES')->whereNull('parent')->where('to_status','Approved')->whereMonth('to_date_from',$m2)->whereYear('to_date_from',$y2)->get();
		
        foreach ($tos as $key => $toss) {
			//check if weekend
			if(!checkIfWeekend($toss['to_date_from']))
			{
				$total_to += $toss['to_total_day'];
			}
            
        }

        //SINGLE DATE
        $tos = App\RequestTO::where('userid',$userid)->where('to_perdiem','YES')->where('parent','YES')->whereIn('to_total_day',[1.0,0.5])->where('to_status','Approved')->whereMonth('to_date_from',$m2)->whereYear('to_date_from',$y2)->get();
		
        foreach ($tos as $key => $toss) {
			//check if weekend
			if(!checkIfWeekend($toss['to_date_from']))
			{
				$total_to += $toss['to_total_day'];
			}
            
        }

        $l_total = $l1_total + $l2_total + $total_to;

		$sa = 2850 - ($l_total * 150);
        //$sa = $salas->sa_amt - ($l_total * 150);
        $la = 500 - ((500 / 22) * ($l1_total + $l2_total));

		if($sa < 0)
		{
			$sa = 0;
		}

		if($la < 0)
		{
			$la = 0;
		}

        $mc = new App\Payroll\MC;
        $mc->payroll_mon = $m;
        $mc->payroll_yr = $y;
        $mc->empcode = $username;
        $mc->salary = $plantilla['plantilla_salary'];
        $mc->userid = $userid;
        $mc->sa = $sa;
        $mc->la = $la;
        $mc->lp = $lp;
        $mc->hmo = $hmo;
        $mc->gsis = $gsis;
        $mc->pmpc = $pmpc;
        $mc->cdc = $cdc;
        $mc->gfal = $gfal;
        $mc->landbank = $lb;
        $mc->itw = $itw;
        $mc->hprate = $hprate;
        $mc->process_code = $process_code;
        $mc->save();



    }

    function checkIfProcess($userid,$mon,$yr)
    {
        $dtr = App\DTRProcessed::where('userid',$userid)->where('dtr_mon',$mon)->where('dtr_year',$yr)->count();

        if($dtr > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function addToMCDays($userid,$empcode,$reqtype,$deduc,$reqdate,$reqdate2,$processcode)
    {
        $mc = new App\MCday;
        $mc->userid = $userid;
        $mc->empcode = $empcode;
        $mc->req_type = $reqtype;
        $mc->req_deduc = $deduc;
        $mc->req_date_from = $reqdate;
        $mc->req_date_to = $reqdate2;
        $mc->process_code = $processcode;
        $mc->save();
    }
}



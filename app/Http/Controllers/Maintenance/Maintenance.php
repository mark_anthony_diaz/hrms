<?php

namespace App\Http\Controllers\Maintenance;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App;
use Carbon\Carbon;
use Auth;

class Maintenance extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $data = [
                    'dtr_option' => collect(App\DTROption::get()),
                    'holiday' => collect(App\Holiday::orderBy('holiday_date','DESC')->get()),
                    'suspension' => collect(App\Suspension::orderBy('fldSuspensionDate','DESC')->get()),
                    'exemption' => collect(App\User::where('dtr_exe',1)->get())
                ];

        return view('dtr.admin.maintenance')->with('data',$data);
    }

    public function workschedule()
    {
        //UPDATE LAST WS
        $ws = new App\WorkSchedule;
        $ws = $ws->where('id',request()->ws_id)
                ->update([
                            'date_to' => date('Y-m-d'),
                        ]);

        $ws = new App\WorkSchedule;
        $ws->dtr_option_id = request()->dtroptions;
        $ws->date_from = date('Y-m-d');
        $ws->save();

        return redirect('maintenance');
    }

    public function library($action)
    {
        $act = explode("-", $action);

        switch ($act[0]) {
            case 'add':
                    switch ($act[1]) {
                        case 'suspension':

                            //TIME
                            switch(request()->sus_time)
                            {
                                case "Wholeday":
                                    $tm = 8;
                                break;

                                case "AM":
                                case "PM":
                                    $tm = 4;
                                break;
                            }

                            $suspension = new App\Suspension;
                            $suspension->fldSuspensionDate =  request()->sus_date;
                            $suspension->fldSuspensionTime =  request()->sus_tm;
                            $suspension->fldMinHrs =  request()->sus_minhr;
                            $suspension->fldSuspensionRemarks =  request()->sus_remarks;
                            $suspension->suspension_time_desc =  request()->sus_time;
                            $suspension->suspension_time =  $tm;
                            $suspension->save();

                        break;
                    }
            break;
        }

        return redirect('maintenance');
    }

    public function reversedtr()
    {
        $code = request()->process_code;
        
        //DELETE PROCESSED
        App\DTRProcessed::where('process_code',$code)->delete();

        //DELETE SALA
        App\Employee_sala::where('process_code',$code)->delete();

        //DELETE MC
        App\Payroll\MC::where('process_code',$code)->delete();

        //DELETE MC DAYS
        App\Payroll\MCDays::where('process_code',$code)->delete();

        //DELETE HP
        App\Employee_hp::where('process_code',$code)->delete();

        //DELETE LEAVES EARNED/DEDUCTED
        App\Employee_leave::where('process_code',$code)->delete(); 

        //RETURN STATUS LEAVES
        App\Request_leave::where('process_code',$code)->update(["process_code" => null]); 

        //RETURN STATUS T.O
        App\RequestTO::where('process_code',$code)->update(["process_code" => null]);

        return redirect('maintenance');

    }
}

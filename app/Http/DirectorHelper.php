<?php

function getRequestForApproval()
{
	
	$list = App\View_request_leave::where('division',Auth::user()->division)->where('parent','YES')->where('leave_action_status','Pending')->get();
	return $list;
}
function getRequestForApprovalDirector()
{
	$list = App\View_request_leave::where('director', 'YES')->where('parent','YES')->where('leave_action_status','Pending')->get();
	return $list;
}

function getRequestForApproval15()
{
	$list = App\View_request_leave::where('leave_deduction','>=',15)->where('parent','YES')->where('leave_action_status','Pending')->get();
	return $list;
}

function getRequestForTOApproval()
{

	if(Auth::user()->usertype == 'Director' && Auth::user()->division == 'O')
	{
		if(Auth::user()->usertype == 'Director')
			{
				$list = App\RequestTO::where('parent','YES')->where('to_status','Pending')
						->where(function($q) {
						          $q->where('division',Auth::user()->division)
						            ->orWhere('director', 'YES');
						      })
						->get();
			}
			else
			{
				$list = App\RequestTO::where('parent','YES')->where('to_status','Pending')
					->where(function($q) {
						          $q->where('division',Auth::user()->division)
						            ->orWhere('director', 'YES');
						      })
					->get();
			}
	}
	else
	{
		if(Auth::user()->usertype == 'Director')
			{
				$list = App\RequestTO::where('parent','YES')->where('director','NO')->where('to_status','Pending')->where('division',Auth::user()->division)->get();
			}
			else
			{
				$list = App\RequestTO::where('parent','YES')->where('to_status','Pending')->where('division',Auth::user()->division)->get();
			}
	}

	return $list;
}

function getRequestForOTApproval()
{
	if(Auth::user()->usertype == 'Marshal' && Auth::user()->division == 'O')
	{
		$list = App\RequestOT::where('director', 'YES')->orWhere('division','O')->where('ot_status','Pending')->get();
	}
	else
	{
		$list = App\RequestOT::where('division',Auth::user()->division)->whereIn('ot_status',['Pending','OED Approved','Time Edited'])->get();
	}
	
	
	return $list;
}





